#ifndef _SERIAL_APP_H_
#define _SERIAL_APP_H_

#ifdef __cplusplus
extern "C"
{
#endif

#if defined(DEBUG_SERIAL)
extern void SerialPrintString(uint8 str[]);
extern void SerialPrintValue(char *title, uint16 value, uint8 format);
#else
#define SerialPrintString(a) 
#define SerialPrintValue(a, b, c)
#endif

#define SBP_UART_PORT                  HAL_UART_PORT_0
//#define SBP_UART_FC                    TRUE
#define SBP_UART_FC                    FALSE
#define SBP_UART_FC_THRESHOLD          48
#define SBP_UART_RX_BUF_SIZE           128
#define SBP_UART_TX_BUF_SIZE           256
#define SBP_UART_IDLE_TIMEOUT          6
#define SBP_UART_INT_ENABLE            FALSE
#define SBP_UART_BR                    HAL_UART_BR_115200


// Serial Port Related
extern void SerialApp_Init( uint8 taskID , uint8 mode, uint8 baudRate);
extern void SerialApp_Init_2( uint8 taskID , uint8 mode, uint8 baudRate);
extern void sbpSerialAppCallback(uint8 port, uint8 event);
extern void sbpSerialAppCallbackTrans(uint8 port, uint8 event);
extern void serialAppInitTransport(uint8 mode, uint8 baudRate);
extern void sbpSerialAppWrite(uint8 *pBuffer, uint16 length);


#ifdef __cplusplus
}
#endif

#endif
