/*
 * This file is no use --- LiXianyu 2015-09-14 Mon.
 */
#include "ac_cfg.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "OSAL.h"
#include "avrdude.h"
#include "avr.h"
#include "pgm.h"
#include "stk500.h"
#include "stk500_private.h"
#include "serial.h"
#include "fileio.h"
#include "lists.h"
#include "pindefs.h"
#include "update.h"
#include "pgm_type.h"
#include "arduino.h"
#include "..\SerialApp.h"
#include "m328p.h"

static void initTheAvrPart_328p(AVRPART *p);

/* Ative delay: 125 cycles ~1 msec */
#define KFD_HAL_DELAY(n) st( { volatile uint32 i; for (i=0; i<(n); i++) { }; } )

PROGRAMMER  ggm;
PROGRAMMER  *pgm;
AVRPART gAvrPart;
int    ovsigck;     /* 1=override sig check, 0=don't */
UPDATE g_upd;

int m328p_recv(unsigned char *buf, size_t len)
{
    return serial_recv(&pgm->fd, buf, len);
}

int m328p_init(void)
{
    int              rc;          /* general return code checking */
    int              exitrc;      /* exit code for main() */
    struct avrpart *p;            /* which avr part we are programming */
    UPDATE          *upd = &g_upd;
    LNODEID         *ln;

    int     erase;       /* 1=erase chip, 0=don't */
    char   *port;        /* device port (/dev/xxx) */
    int32   baudrate;    /* override default programmer baud rate */
    double  bitclock;    /* Specify programmer bit clock (JTAG ICE) */
    int     ispdelay;    /* Specify the delay for ISP clock */
    int     safemode;    /* Enable safemode, 1=safemode on, 0=normal */
    int     silentsafe;  /* Don't ask about fuses, 1=silent, 0=normal */
    int     init_ok = 0;     /* Device initialization worked well */
    int     is_open;     /* Device open succeeded */
    enum updateflags uflags = UF_AUTO_ERASE; /* Flags for do_op() */
    unsigned char safemode_lfuse = 0xff;
    unsigned char safemode_hfuse = 0xff;
    unsigned char safemode_efuse = 0xff;
    unsigned char safemode_fuse = 0xff;

    char *safemode_response;
    int fuses_specified = 0;
    int fuses_updated = 0;

    SerialPrintString("...000\r\n");

    p             = NULL;
    ovsigck       = 0;
    pgm           = NULL;
    baudrate      = 0;
    bitclock      = 0.0;
    ispdelay      = 0;
    safemode      = 1;       /* Safemode on by default */
    silentsafe    = 0;       /* Ask by default */
    is_open       = 0;
    
    //-p
    //partdesc = "atmega328p";

    //-c
    //programmer = "arduino";

    //-b
    baudrate = 115200;

    //-D
    uflags &= ~UF_AUTO_ERASE;/* disable auto erase */

    //-U
    char myoptarg[22] = {0};
    //strcpy(myoptarg, "flash:w:C:\\Users\\Blink.hex:i");
    strcpy(myoptarg, "flash:w:Blink.hex:i");
    parse_op_elara(upd);
    //upd = (UPDATE *)osal_mem_alloc(sizeof(UPDATE));
    if (upd == NULL)
    {
        //      fprintf(stderr, "%s: error parsing update operation '%s'\n", progname, optarg);
        //      exit(1);
    }
    
    //ladd(updates, upd);
    pgm = &ggm;
    init_programmer(pgm);
    SerialPrintString("333............\r\n");
    pgm->initpgm = &arduino_initpgm;
    if (pgm->initpgm)
    {
        pgm->initpgm(pgm);
    }
    else
    {
        //fprintf(stderr,
        //        "\n%s: Can't initialize the programmer.\n\n",
        //        progname);
        //exit(1);
    }
    SerialPrintString("444............\r\n");
    if (pgm->setup)
    {
        pgm->setup(pgm);
    }
    SerialPrintString("555............\r\n");

    memset(&gAvrPart, 0, sizeof(AVRPART));
    p = &gAvrPart;
    SerialPrintString("666............\r\n");
    initTheAvrPart_328p(p);
    
    safemode = 0;

    if (avr_initmem(p) != 0)
    {
        //fprintf(stderr, "\n%s: failed to initialize memories\n",
        //        progname);
        //exit(1);
    }
    
    if (baudrate != 0)
    {
        //if (verbose) {
        //  fprintf(stderr, "%sOverriding Baud Rate          : %d\n", progbuf, baudrate);
        //}
        pgm->baudrate = baudrate;
    }

    if (bitclock != 0.0)
    {
        //if (verbose)
        {
            //fprintf(stderr, "%sSetting bit clk period        : %.1f\n", progbuf, bitclock);
        }
        pgm->bitclock = bitclock * 1e-6;
    }

    if (ispdelay != 0)
    {
        //if (verbose)
        {
            //fprintf(stderr, "%sSetting isp clock delay        : %3i\n", progbuf, ispdelay);
        }
        pgm->ispdelay = ispdelay;
    }
    return 0;
}

int m328p_open(void)
{
    char *port = "HAL_UART_PORT_0";
    int rc = pgm->open(pgm, port);
    SerialPrintString("10............\r\n");
    return rc;
}

// See stk500_getsync
int m328p_getsync(void)
{
    unsigned char buf[16] = {0};
    /*
     * get in sync */
    buf[0] = Cmnd_STK_GET_SYNC;
    buf[1] = Sync_CRC_EOP;

    /*
     * First send and drain a few times to get rid of line noise
     */

    //stk500_send(pgm, buf, 2);
    return serial_send(&pgm->fd, buf, 2);
}

// See stk500_getparm
// Parm_STK_SW_MAJOR and Parm_STK_SW_MINOR
int m328p_getparm(unsigned parm)
{
    unsigned char buf[16] = {0};
    buf[0] = Cmnd_STK_GET_PARAMETER;
    buf[1] = parm;
    buf[2] = Sync_CRC_EOP;

    //stk500_send(pgm, buf, 3);
    return serial_send(&pgm->fd, buf, 3);
}

int m328p_initialize(void)
{
    /*
     * enable the programmer
     */
    pgm->enable(pgm);

    /*
     * turn off all the status leds
     */
    pgm->rdy_led(pgm, OFF);
    pgm->err_led(pgm, OFF);
    pgm->pgm_led(pgm, OFF);
    pgm->vfy_led(pgm, OFF);

    /*
     * initialize the chip in preperation for accepting commands
     */
    enum updateflags uflags = UF_AUTO_ERASE; /* Flags for do_op() */
    // -D
    uflags &= ~UF_AUTO_ERASE;/* disable auto erase */
    int exitrc = 0;
    int rc = 0;
    int erase = 0;
    int init_ok = (rc = pgm->initialize(pgm, &gAvrPart)) >= 0;
    SerialPrintString("38............\r\n");
    if (!init_ok)
    {
        SerialPrintString("39............\r\n");
        //fprintf(stderr, "%s: initialization failed, rc=%d\n", progname, rc);
        #if 1
        if (!ovsigck)
        {
            SerialPrintString("40............\r\n");
            //fprintf(stderr, "%sDouble check connections and try again, "
            //        "or use -F to override\n"
            //        "%sthis check.\n\n",
            //        progbuf, progbuf);
            //exitrc = 1;
            exitrc = rc;
            goto main_exit;
        }
        #endif
    }
    SerialPrintString("41............\r\n");
    /* indicate ready */
    pgm->rdy_led(pgm, ON);
    
    if (init_ok && erase)
    {
        SerialPrintString("42............\r\n");
        /*
         * erase the chip's flash and eeprom memories, this is required
         * before the chip can accept new programming
         */
        if (uflags & UF_NOWRITE)
        {
            SerialPrintString("43............\r\n");
            //fprintf(stderr,
            //    "%s: conflicting -e and -n options specified, NOT erasing chip\n",
            //    progname);
        }
        else
        {
            SerialPrintString("44............\r\n");
            //if (quell_progress < 2)
            {
                //fprintf(stderr, "%s: erasing chip\n", progname);
            }
            exitrc = avr_chip_erase(pgm, &gAvrPart);
            if(exitrc) goto main_exit;
        }
    }
    SerialPrintString("45............\r\n");

main_exit:
    return exitrc;
}

int m328p_do_op(void)
{
    enum updateflags uflags = UF_AUTO_ERASE; /* Flags for do_op() */
    int rc = do_op(pgm, &gAvrPart, &g_upd, uflags);
    SerialPrintString("48............\r\n");
    /*
     * program complete
     */
    {
        SerialPrintString("Leave avr_init()\r\n");
        pgm->powerdown(pgm);// Do nothing --- lxy added.

        pgm->disable(pgm);

        pgm->rdy_led(pgm, OFF);

        pgm->close(pgm);
    }
    SerialPrintString("49............\r\n");
    return rc;
}

static int parse_cmdbits2(OPCODE *op, int bitno, char *ch)
{
    return -1;
    char aStr[3];
    char *q;
    char *e;
    aStr[0] = *ch;
    aStr[1] = *(ch + 1);
    aStr[2] = 0;

    if (aStr[0] == 'a')
    {
        q = &aStr[1];
        op->bit[bitno].bitno = strtol(q, &e, 0);
#if 0
        if ((e == q) || (*e != 0))
        {
            fprintf(stderr,
                    "%s: error at %s:%d: can't parse bit number from \"%s\"\n",
                    progname, infile, lineno, q);
            //exit(1);
            return -1;
        }
#endif
        op->bit[bitno].type = AVR_CMDBIT_ADDRESS;
        op->bit[bitno].value = 0;
    }
    return 0;
}

static int parse_cmdbits3(OPCODE *op, int bitno, char *ch)
{
    return -3;
    char aStr[4];
    char *q;
    char *e;
    aStr[0] = *ch;
    aStr[1] = *(ch + 1);
    aStr[2] = *(ch + 2);
    aStr[3] = 0;

    if (aStr[0] == 'a')
    {
        q = &aStr[1];
        op->bit[bitno].bitno = strtol(q, &e, 0);
#if 0
        if ((e == q) || (*e != 0))
        {
            fprintf(stderr,
                    "%s: error at %s:%d: can't parse bit number from \"%s\"\n",
                    progname, infile, lineno, q);
            //exit(1);
            return -1;
        }
#endif
        op->bit[bitno].type = AVR_CMDBIT_ADDRESS;
        op->bit[bitno].value = 0;
    }
    return 0;
}

static int parse_cmdbits(OPCODE *op, int bitno, char ch)
{
    return -1;
    switch (ch)
    {
    case '1':
        op->bit[bitno].type  = AVR_CMDBIT_VALUE;
        op->bit[bitno].value = 1;
        op->bit[bitno].bitno = bitno % 8;
        break;
    case '0':
        op->bit[bitno].type  = AVR_CMDBIT_VALUE;
        op->bit[bitno].value = 0;
        op->bit[bitno].bitno = bitno % 8;
        break;
    case 'x':
        op->bit[bitno].type  = AVR_CMDBIT_IGNORE;
        op->bit[bitno].value = 0;
        op->bit[bitno].bitno = bitno % 8;
        break;
    case 'a':
        op->bit[bitno].type  = AVR_CMDBIT_ADDRESS;
        op->bit[bitno].value = 0;
        op->bit[bitno].bitno = 8 * (bitno / 8) + bitno % 8;
        break;
    case 'i':
        op->bit[bitno].type  = AVR_CMDBIT_INPUT;
        op->bit[bitno].value = 0;
        op->bit[bitno].bitno = bitno % 8;
        break;
    case 'o':
        op->bit[bitno].type  = AVR_CMDBIT_OUTPUT;
        op->bit[bitno].value = 0;
        op->bit[bitno].bitno = bitno % 8;
        break;
    default :
        //fprintf(stderr,
        //        "%s: error at %s:%d: invalid bit specifier '%c'\n",
        //        progname, infile, lineno, ch);
        //exit(1);
        break;
    }
    return 0;
}

unsigned char const g_eeprom_instr[EEPROM_INSTR_SIZE] = {0xBD, 0xF2, 0xBD, 0xE1, 0xBB, 0xCF, 0xB4, 0x00,
                                                         0xBE, 0x01, 0xB6, 0x01, 0xBC, 0x00, 0xBB, 0xBF,
                                                         0x99, 0xF9, 0xBB, 0xAF
                                                        };
unsigned char const g_controlstack[CTL_STACK_SIZE] = {0x0E, 0x1E, 0x0F, 0x1F, 0x2E, 0x3E, 0x2F,
                                                      0x3F, 0x4E, 0x5E, 0x4F, 0x5F, 0x6E, 0x7E, 0x6F, 0x7F,
                                                      0x66, 0x76, 0x67, 0x77, 0x6A, 0x7A, 0x6B, 0x7B, 0xBE,
                                                      0xFD, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00
                                                     };

AVRMEM g_mem[8];
LIST g_memList;
const OPCODE g_op_eeprom_read = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
const OPCODE g_op_eeprom_write = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,1,6},{1,1,7},
}};
const OPCODE g_op_eeprom_LOADPAGE_LO = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {2,0,0},{2,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,1,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,1,6},{1,1,7},
}};
const OPCODE g_op_eeprom_WRITEPAGE = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,1,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,1,6},{1,1,7},
}};
uint8 g_eeprom_buf[128];
uint8 g_eeprom_tags[128];
void init_memtype_eeprom(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_eeprom()\r\n");

    current_mem->buf = g_eeprom_buf;
    current_mem->tags = g_eeprom_tags;
    
    strncpy(current_mem->desc, "eeprom", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->paged = 0;

    current_mem->page_size = 4;

    current_mem->size = 1024;

    current_mem->min_write_delay = 3600;

    current_mem->max_write_delay = 3600;

    current_mem->readback[0] = 0xff;
    current_mem->readback[1] = 0xff;

    current_mem->mode = 0x41;

    current_mem->delay = 20;

    current_mem->blocksize = 4;

    current_mem->readsize = 256;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_eeprom_read;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits2(op, bitno--, "a9");
    parse_cmdbits2(op, bitno--, "a8");

    parse_cmdbits2(op, bitno--, "a7");
    parse_cmdbits2(op, bitno--, "a6");
    parse_cmdbits2(op, bitno--, "a5");
    parse_cmdbits2(op, bitno--, "a4");
    parse_cmdbits2(op, bitno--, "a3");
    parse_cmdbits2(op, bitno--, "a2");
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;

    uint16 usedmem = 0;
#if defined(DEBUG_SERIAL)
    usedmem = osal_heap_mem_used();
    SerialPrintValue("Now BLE used memory is : ", usedmem, 10);
#endif
    //.........................................
    opnum = AVR_OP_WRITE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_eeprom_write;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
#if defined(DEBUG_SERIAL)
    usedmem = osal_heap_mem_used();
    SerialPrintValue("Now BLE used memory is : ", usedmem, 10);
#endif
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits2(op, bitno--, "a9");
    parse_cmdbits2(op, bitno--, "a8");

    parse_cmdbits2(op, bitno--, "a7");
    parse_cmdbits2(op, bitno--, "a6");
    parse_cmdbits2(op, bitno--, "a5");
    parse_cmdbits2(op, bitno--, "a4");
    parse_cmdbits2(op, bitno--, "a3");
    parse_cmdbits2(op, bitno--, "a2");
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');

    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_LOADPAGE_LO;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_eeprom_LOADPAGE_LO;
    SerialPrintValue("AVR_OP_LOADPAGE_LO op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');

    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITEPAGE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_eeprom_WRITEPAGE;
    SerialPrintValue("AVR_OP_WRITEPAGE op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits2(op, bitno--, "a9");
    parse_cmdbits2(op, bitno--, "a8");

    parse_cmdbits2(op, bitno--, "a7");
    parse_cmdbits2(op, bitno--, "a6");
    parse_cmdbits2(op, bitno--, "a5");
    parse_cmdbits2(op, bitno--, "a4");
    parse_cmdbits2(op, bitno--, "a3");
    parse_cmdbits2(op, bitno--, "a2");
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    current_mem->op[opnum] = op;
}

const OPCODE g_op_flash_READ_LO = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{2,0,10},{2,0,11},{2,0,12},{2,0,13},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,1,5},{1,0,6},{1,0,7},
}};
const OPCODE g_op_flash_READ_HI = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{2,0,10},{2,0,11},{2,0,12},{2,0,13},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,0,7},
}};
const OPCODE g_op_flash_LOADPAGE_LO = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,1,6},{1,0,7},
}};
const OPCODE g_op_flash_LOADPAGE_HI = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,0,5},{1,1,6},{1,0,7},
}};
const OPCODE g_op_flash_WRITEPAGE = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{2,0,10},{2,0,11},{2,0,12},{2,0,13},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,0,5},{1,1,6},{1,0,7},
}};
uint8 g_blink_bin[282] = {
    0x0c,0x94,0x34,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x11,0x24,0x1f,0xbe,0xcf,0xef,0xd8,0xe0,
    0xde,0xbf,0xcd,0xbf,0x0e,0x94,0x40,0x00,0x0c,0x94,0x8b,0x00,0x0c,0x94,0x00,0x00,
    0xcf,0x93,0xdf,0x93,0x00,0xd0,0x00,0xd0,0xcd,0xb7,0xde,0xb7,0x84,0xe2,0x90,0xe0,
    0x20,0xe2,0xfc,0x01,0x20,0x83,0x85,0xe2,0x90,0xe0,0x20,0xe2,0xfc,0x01,0x20,0x83,
    0x19,0x82,0x1a,0x82,0x1b,0x82,0x1c,0x82,0x0b,0xc0,0x89,0x81,0x9a,0x81,0xab,0x81,
    0xbc,0x81,0x01,0x96,0xa1,0x1d,0xb1,0x1d,0x89,0x83,0x9a,0x83,0xab,0x83,0xbc,0x83,
    0x89,0x81,0x9a,0x81,0xab,0x81,0xbc,0x81,0x80,0x3a,0xf6,0xe8,0x9f,0x07,0xf1,0xe0,
    0xaf,0x07,0xb1,0x05,0x54,0xf3,0x85,0xe2,0x90,0xe0,0xfc,0x01,0x10,0x82,0x19,0x82,
    0x1a,0x82,0x1b,0x82,0x1c,0x82,0x0b,0xc0,0x89,0x81,0x9a,0x81,0xab,0x81,0xbc,0x81,
    0x01,0x96,0xa1,0x1d,0xb1,0x1d,0x89,0x83,0x9a,0x83,0xab,0x83,0xbc,0x83,0x89,0x81,
    0x9a,0x81,0xab,0x81,0xbc,0x81,0x80,0x3a,0xf6,0xe8,0x9f,0x07,0xf1,0xe0,0xaf,0x07,
    0xb1,0x05,0x54,0xf3,0xc0,0xcf,0xf8,0x94,0xff,0xcf
};
uint8 g_flash_buf[300];
uint8 g_flash_tags[300];
void init_memtype_flash(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_flash()\r\n");
    
//    current_mem->buf = (unsigned char*)osal_mem_alloc(128);
//    current_mem->tags = (unsigned char*)osal_mem_alloc(128);
    memset(g_flash_buf, 0, sizeof(g_flash_buf));
    memcpy(g_flash_buf, (void*)g_blink_bin, sizeof(g_blink_bin));
    current_mem->buf = g_flash_buf;
    current_mem->tags = g_flash_tags;
    
    SerialPrintValue("buf = 0x",(uint16)current_mem->buf, 16);
    SerialPrintValue("tags = 0x",(uint16)current_mem->tags, 16);
    
    strncpy(current_mem->desc, "flash", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->paged = 1;

    current_mem->size = 32768;

    current_mem->page_size = 128;

    current_mem->num_pages = 256;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    current_mem->readback[0] = 0xff;
    current_mem->readback[1] = 0xff;

    current_mem->mode = 0x41;

    current_mem->delay = 6;

    current_mem->blocksize = 128;

    current_mem->readsize = 256;

    opnum = AVR_OP_READ_LO;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_flash_READ_LO;
    SerialPrintValue("AVR_OP_READ_LO op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits3(op, bitno--, "a13");
    parse_cmdbits3(op, bitno--, "a12");
    parse_cmdbits3(op, bitno--, "a11");
    parse_cmdbits3(op, bitno--, "a10");
    parse_cmdbits2(op, bitno--, "a9");
    parse_cmdbits2(op, bitno--, "a8");

    parse_cmdbits2(op, bitno--, "a7");
    parse_cmdbits2(op, bitno--, "a6");
    parse_cmdbits2(op, bitno--, "a5");
    parse_cmdbits2(op, bitno--, "a4");
    parse_cmdbits2(op, bitno--, "a3");
    parse_cmdbits2(op, bitno--, "a2");
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');

    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_READ_HI;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_flash_READ_HI;
    SerialPrintValue("AVR_OP_READ_HI op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits3(op, bitno--, "a13");
    parse_cmdbits3(op, bitno--, "a12");
    parse_cmdbits3(op, bitno--, "a11");
    parse_cmdbits3(op, bitno--, "a10");
    parse_cmdbits2(op, bitno--, "a9");
    parse_cmdbits2(op, bitno--, "a8");

    parse_cmdbits2(op, bitno--, "a7");
    parse_cmdbits2(op, bitno--, "a6");
    parse_cmdbits2(op, bitno--, "a5");
    parse_cmdbits2(op, bitno--, "a4");
    parse_cmdbits2(op, bitno--, "a3");
    parse_cmdbits2(op, bitno--, "a2");
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_LOADPAGE_LO;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_flash_LOADPAGE_LO;
    SerialPrintValue("AVR_OP_LOADPAGE_LO op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits2(op, bitno--, "a5");
    parse_cmdbits2(op, bitno--, "a4");
    parse_cmdbits2(op, bitno--, "a3");
    parse_cmdbits2(op, bitno--, "a2");
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_LOADPAGE_HI;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_flash_LOADPAGE_HI;
    SerialPrintValue("AVR_OP_LOADPAGE_HI op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits2(op, bitno--, "a5");
    parse_cmdbits2(op, bitno--, "a4");
    parse_cmdbits2(op, bitno--, "a3");
    parse_cmdbits2(op, bitno--, "a2");
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITEPAGE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_flash_WRITEPAGE;
    SerialPrintValue("AVR_OP_WRITEPAGE op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits3(op, bitno--, "a13");
    parse_cmdbits3(op, bitno--, "a12");
    parse_cmdbits3(op, bitno--, "a11");
    parse_cmdbits3(op, bitno--, "a10");
    parse_cmdbits2(op, bitno--, "a9");
    parse_cmdbits2(op, bitno--, "a8");

    parse_cmdbits2(op, bitno--, "a7");
    parse_cmdbits2(op, bitno--, "a6");
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    current_mem->op[opnum] = op;
}

const OPCODE g_op_lfuse_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
}};
const OPCODE g_op_lfuse_WRITE = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
void init_memtype_lfuse(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_lfuse()\r\n");
    strncpy(current_mem->desc, "lfuse", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_lfuse_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_lfuse_WRITE;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    current_mem->op[opnum] = op;
}

const OPCODE g_op_hfuse_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
}};
const OPCODE g_op_hfuse_WRITE = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
void init_memtype_hfuse(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_hfuse()\r\n");
    strncpy(current_mem->desc, "hfuse", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_hfuse_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_hfuse_WRITE;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    current_mem->op[opnum] = op;
}

const OPCODE g_op_efuse_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
}};
const OPCODE g_op_efuse_WRITE = {{
    {3,0,0},{3,0,1},{3,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,1,2},{1,0,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
void init_memtype_efuse(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_efuse()\r\n");
    strncpy(current_mem->desc, "efuse", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_efuse_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_efuse_WRITE;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    current_mem->op[opnum] = op;
}

const OPCODE g_op_lock_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
}};
const OPCODE g_op_lock_WRITE = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{1,1,6},{1,1,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,1,5},{1,1,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
void init_memtype_lock(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_lock()\r\n");
    strncpy(current_mem->desc, "lock", AVR_MEMDESCLEN);
    SerialPrintString(current_mem->desc);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_lock_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_lock_WRITE;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    current_mem->op[opnum] = op;
}

const OPCODE g_op_calibration_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,1,4},{1,1,5},{1,0,6},{1,0,7},
}};
void init_memtype_calibration(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_calibration()\r\n");
    strncpy(current_mem->desc, "calibration", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_calibration_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;
}

const OPCODE g_op_signature_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {2,0,0},{2,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,1,4},{1,1,5},{1,0,6},{1,0,7},
}};
void init_memtype_signature(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_signature()\r\n");
    strncpy(current_mem->desc, "signature", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 3;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_signature_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;
}

const OPCODE g_op_part_PGM_ENABLE = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,1,0},{1,1,1},{1,0,2},{1,0,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
const OPCODE g_op_part_CHIP_ERASE = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
static void initTheAvrPart_328p(AVRPART *current_part)
{
    SerialPrintValue("Enter initTheAvrPart_328p() current_part = 0x",(uint16)current_part, 16);
    
    current_part->reset_disposition = RESET_DEDICATED;
    current_part->retry_pulse = PIN_AVR_SCK;
    current_part->flags = AVRPART_SERIALOK | AVRPART_ENABLEPAGEPROGRAMMING;
    memset(current_part->signature, 0xFF, 3);
    current_part->ctl_stack_type = CTL_STACK_NONE;
    current_part->ocdrev = -1;
    current_part->mem = lcreat(&g_memList, 8);
    SerialPrintValue("mem = 0x", (uint16)current_part->mem, 16);
    int nbytes = 0;
    strncpy(current_part->id, "m328p", AVR_IDLEN);
    current_part->id[AVR_IDLEN - 1] = 0;

    strncpy(current_part->desc, "ATmega328P", AVR_DESCLEN);
    current_part->desc[AVR_DESCLEN - 1] = 0;

    current_part->signature[0] = 0x1e;
    current_part->signature[1] = 0x95;
    current_part->signature[2] = 0x0F;

    current_part->ocdrev = 1;

    current_part->flags |= AVRPART_HAS_DW;

    current_part->flash_instr[nbytes++] = 0xB6;
    current_part->flash_instr[nbytes++] = 0x01;
    current_part->flash_instr[nbytes++] = 0x01;

    memcpy(current_part->eeprom_instr, (void *)g_eeprom_instr, EEPROM_INSTR_SIZE);

    current_part->stk500_devcode = 0x86;

    current_part->pagel = 0xd7;

    current_part->bs2 = 0xc2;

    current_part->chip_erase_delay = 9000;

    int opnum = AVR_OP_PGM_ENABLE;
    SerialPrintString("777............\r\n");
    //OPCODE *op = avr_new_opcode();
    OPCODE *op = (OPCODE*)&g_op_part_PGM_ENABLE;
    
    int bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    current_part->op[opnum] = op;
    ////////////////////////////////////////////////////////////////////////////
    opnum = AVR_OP_CHIP_ERASE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_part_CHIP_ERASE;
    SerialPrintValue("AVR_OP_CHIP_ERASE op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    current_part->op[opnum] = op;

    current_part->timeout = 200;

    current_part->stabdelay = 100;

    current_part->cmdexedelay = 25;

    current_part->synchloops = 32;

    current_part->bytedelay = 0;

    current_part->pollindex = 3;

    current_part->pollvalue = 0x53;

    current_part->predelay = 1;

    current_part->postdelay = 1;

    current_part->pollmethod = 1;

    memcpy(current_part->controlstack, (void *)g_controlstack, CTL_STACK_SIZE);

    current_part->hventerstabdelay = 100;

    current_part->progmodedelay = 0;

    current_part->latchcycles = 5;

    current_part->togglevtg = 1;

    current_part->poweroffdelay = 15;

    current_part->resetdelayms = 1;

    current_part->resetdelayus = 0;

    current_part->hvleavestabdelay = 15;

    current_part->resetdelay = 15;

    current_part->chiperasepulsewidth = 0;

    current_part->chiperasepolltimeout = 10;

    current_part->programfusepulsewidth = 0;

    current_part->programfusepolltimeout = 5;

    current_part->programlockpulsewidth = 0;

    current_part->programlockpolltimeout = 5;

    /////////////////////////////////////////////////////////////////////
    AVRMEM *current_mem;
    //AVRMEM *current_mem = avr_new_memtype();
    current_mem = &g_mem[0];
    init_memtype_eeprom(current_mem);
    ladd(current_part->mem, current_mem);
    /////////////////////////////////////////////////////////////////////

    SerialPrintString("888............\r\n");
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[1];
    init_memtype_flash(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[2];
    init_memtype_lfuse(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[3];
    init_memtype_hfuse(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[4];
    init_memtype_efuse(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[5];
    init_memtype_lock(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[6];
    init_memtype_calibration(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[7];
    init_memtype_signature(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////

    SerialPrintString("9............\r\n");
}
