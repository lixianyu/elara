#ifndef MAIN_COMMON_H
#define MAIN_COMMON_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

#include "hal_types.h"
#include "avrpart.h"
/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */
//extern AVRMEM g_mem[8];
extern AVRMEM g_mem0,g_mem1,g_mem2,g_mem3,g_mem4,g_mem5,g_mem6,g_mem7;
extern LIST g_memList;
extern uint8 g_flash_buf[300];
/*********************************************************************
 * FUNCTIONS
 */
extern int avr_init_common(char *partId);
extern int justForTestFlash(void);
/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif
