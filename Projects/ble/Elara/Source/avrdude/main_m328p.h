#ifndef MAIN_M328P_H
#define MAIN_M328P_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

#include "hal_types.h"

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * FUNCTIONS
 */
extern void initAvrPart_m328p(AVRPART *current_part, char* partId);
/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif
