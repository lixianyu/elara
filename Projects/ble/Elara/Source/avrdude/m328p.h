#ifndef _STK_CC2541_APP_H_
#define _STK_CC2541_APP_H_

extern int m328p_open(void);
extern int m328p_recv(unsigned char *buf, size_t len);
extern int m328p_getsync(void);
extern int m328p_getparm(unsigned parm);
#endif
