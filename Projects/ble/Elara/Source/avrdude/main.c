/*
 * avrdude - A Downloader/Uploader for AVR device programmers
 * Copyright (C) 2000-2005  Brian S. Dean <bsd@bsdhome.com>
 * Copyright 2007-2014 Joerg Wunsch <j@uriah.heep.sax.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* $Id: main.c 1294 2014-03-12 23:03:18Z joerg_wunsch $ */

/*
 * Code to program an Atmel AVR device through one of the supported
 * programmers.
 *
 * For parallel port connected programmers, the pin definitions can be
 * changed via a config file.  See the config file for instructions on
 * how to add a programmer definition.
 *
 */

#include "ac_cfg.h"

//#include <stdio.h>
#include <stdlib.h>
//#include <errno.h>
//#include <fcntl.h>
//#include <limits.h>
#include <string.h>
//#include <time.h>
//#include <unistd.h>
//#include <ctype.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <sys/time.h>
#include "OSAL.h"
#include "avr.h"
//#include "config.h"
//#include "confwin.h"
#include "fileio.h"
#include "lists.h"
//#include "par.h"
#include "pindefs.h"
//#include "term.h"
////#include "safemode.h"
#include "update.h"
#include "pgm_type.h"

#include "arduino.h"
/* Get VERSION from ac_cfg.h */
//char * version      = VERSION;
#include "..\SerialApp.h"
#include "..\elara_file_common.h"

#define fprintf(s, ...)

char *progname;

static void initTheAvrPart_328p(AVRPART *p);
#if 0
char   progbuf[PATH_MAX]; /* temporary buffer of spaces the same
                             length as progname; used for lining up
                             multiline messages */

struct list_walk_cookie
{
    FILE *f;
    const char *prefix;
};
#endif
//static LISTID updates = NULL;

//static LISTID extended_params = NULL;

//static LISTID additional_config_files = NULL;

static PROGRAMMER  ggm;
static PROGRAMMER  *pgm;
static AVRPART gAvrPart;

/*
 * global options
 */
//int    verbose;     /* verbose output */
//int    quell_progress; /* un-verebose output */
static int    ovsigck;     /* 1=override sig check, 0=don't */

#define __tolower(c)    ((('A' <= (c))&&((c) <= 'Z')) ? ((c) - 'A' + 'a') : (c))
static int strcasecmp(const char *s1, const char *s2)
{
    const unsigned char *p1 = (const unsigned char *) s1;
    const unsigned char *p2 = (const unsigned char *) s2;
    int result = 0;

    if (p1 == p2)
    {
        return 0;
    }

    while ((result = __tolower(*p1) - __tolower(*p2)) == 0)
    {
        if (*p1++ == '\0')
        {
            break;
        }
        p2++;
    }

    return result;
}

#if 0
/*
 * usage message
 */
static void usage(void)
{
    fprintf(stderr,
            "Usage: %s [options]\n"
            "Options:\n"
            "  -p <partno>                Required. Specify AVR device.\n"
            "  -b <baudrate>              Override RS-232 baud rate.\n"
            "  -B <bitclock>              Specify JTAG/STK500v2 bit clock period (us).\n"
            "  -C <config-file>           Specify location of configuration file.\n"
            "  -c <programmer>            Specify programmer type.\n"
            "  -D                         Disable auto erase for flash memory\n"
            "  -i <delay>                 ISP Clock Delay [in microseconds]\n"
            "  -P <port>                  Specify connection port.\n"
            "  -F                         Override invalid signature check.\n"
            "  -e                         Perform a chip erase.\n"
            "  -O                         Perform RC oscillator calibration (see AVR053). \n"
            "  -U <memtype>:r|w|v:<filename>[:format]\n"
            "                             Memory operation specification.\n"
            "                             Multiple -U options are allowed, each request\n"
            "                             is performed in the order specified.\n"
            "  -n                         Do not write anything to the device.\n"
            "  -V                         Do not verify.\n"
            "  -u                         Disable safemode, default when running from a script.\n"
            "  -s                         Silent safemode operation, will not ask you if\n"
            "                             fuses should be changed back.\n"
            "  -t                         Enter terminal mode.\n"
            "  -E <exitspec>[,<exitspec>] List programmer exit specifications.\n"
            "  -x <extended_param>        Pass <extended_param> to programmer.\n"
            "  -y                         Count # erase cycles in EEPROM.\n"
            "  -Y <number>                Initialize erase cycle # in EEPROM.\n"
            "  -v                         Verbose output. -v -v for more.\n"
            "  -q                         Quell progress output. -q -q for less.\n"
            "  -l logfile                 Use logfile rather than stderr for diagnostics.\n"
            "  -?                         Display this usage.\n"
            "\navrdude version %s, URL: <http://savannah.nongnu.org/projects/avrdude/>\n"
            , progname, version);
}


static void update_progress_tty (int percent, double etime, char *hdr)
{
    static char hashes[51];
    static char *header;
    static int last = 0;
    int i;

    setvbuf(stderr, (char *)NULL, _IONBF, 0);

    hashes[50] = 0;

    memset (hashes, ' ', 50);
    for (i = 0; i < percent; i += 2)
    {
        hashes[i / 2] = '#';
    }

    if (hdr)
    {
        fprintf (stderr, "\n");
        last = 0;
        header = hdr;
    }

    if (last == 0)
    {
        fprintf(stderr, "\r%s | %s | %d%% %0.2fs",
                header, hashes, percent, etime);
    }

    if (percent == 100)
    {
        if (!last) fprintf (stderr, "\n\n");
        last = 1;
    }

    setvbuf(stderr, (char *)NULL, _IOLBF, 0);
}

static void update_progress_no_tty (int percent, double etime, char *hdr)
{
    static int done = 0;
    static int last = 0;
    int cnt = (percent >> 1) * 2;

    setvbuf(stderr, (char *)NULL, _IONBF, 0);

    if (hdr)
    {
        fprintf (stderr, "\n%s | ", hdr);
        last = 0;
        done = 0;
    }
    else
    {
        while ((cnt > last) && (done == 0))
        {
            fprintf (stderr, "#");
            cnt -=  2;
        }
    }

    if ((percent == 100) && (done == 0))
    {
        fprintf (stderr, " | 100%% %0.2fs\n\n", etime);
        last = 0;
        done = 1;
    }
    else
        last = (percent >> 1) * 2; /* Make last a multiple of 2. */

    setvbuf(stderr, (char *)NULL, _IOLBF, 0);
}

static void list_programmers_callback(const char *name, const char *desc,
                                      const char *cfgname, int cfglineno,
                                      void *cookie)
{
    struct list_walk_cookie *c = (struct list_walk_cookie *)cookie;
    if (verbose)
    {
        fprintf(c->f, "%s%-16s = %-30s [%s:%d]\n",
                c->prefix, name, desc, cfgname, cfglineno);
    }
    else
    {
        fprintf(c->f, "%s%-16s = %-s\n",
                c->prefix, name, desc);
    }
}

static void list_programmers(FILE *f, const char *prefix, LISTID programmers)
{
    struct list_walk_cookie c;

    c.f = f;
    c.prefix = prefix;

    sort_programmers(programmers);

    walk_programmers(programmers, list_programmers_callback, &c);
}

static void list_programmer_types_callback(const char *name, const char *desc,
        void *cookie)
{
    struct list_walk_cookie *c = (struct list_walk_cookie *)cookie;
    fprintf(c->f, "%s%-16s = %-s\n",
            c->prefix, name, desc);
}

static void list_programmer_types(FILE *f, const char *prefix)
{
    struct list_walk_cookie c;

    c.f = f;
    c.prefix = prefix;

    walk_programmer_types(list_programmer_types_callback, &c);
}

static void list_avrparts_callback(const char *name, const char *desc,
                                   const char *cfgname, int cfglineno,
                                   void *cookie)
{
    struct list_walk_cookie *c = (struct list_walk_cookie *)cookie;

    /* hide ids starting with '.' */
    if ((verbose < 2) && (name[0] == '.'))
        return;

    if (verbose)
    {
        fprintf(c->f, "%s%-8s = %-18s [%s:%d]\n",
                c->prefix, name, desc, cfgname, cfglineno);
    }
    else
    {
        fprintf(c->f, "%s%-8s = %s\n",
                c->prefix, name, desc);
    }
}

static void list_parts(FILE *f, const char *prefix, LISTID avrparts)
{
    struct list_walk_cookie c;

    c.f = f;
    c.prefix = prefix;

    sort_avrparts(avrparts);

    walk_avrparts(avrparts, list_avrparts_callback, &c);
}
#endif

static void exithook(void)
{
    if (pgm->teardown)
        pgm->teardown(pgm);
}

static void cleanup_main(void)
{
#if 0
    if (updates)
    {
        ldestroy_cb(updates, (void(*)(void *))free_update);
        updates = NULL;
    }

    if (extended_params)
    {
        ldestroy(extended_params);
        extended_params = NULL;
    }
    if (additional_config_files)
    {
        ldestroy(additional_config_files);
        additional_config_files = NULL;
    }
#endif
    //    cleanup_config();
}


/*
 *    ��ʱ����
 *    ����΢��
 */
static void delay_nus(uint32 timeout)
{
    while (timeout--)
    {
        asm("NOP");
        asm("NOP");
        asm("NOP");
    }
}

static UPDATE g_upd;

/*
 * main routine
 */
//int main(int argc, char * argv [])
int avr_init(void)
{
    int              rc;          /* general return code checking */
    int              exitrc;      /* exit code for main() */
    //int              i;           /* general loop counter */
    //int              ch;          /* options flag */
    //int              len;         /* length for various strings */
    struct avrpart *p;            /* which avr part we are programming */
    //AVRMEM          *sig;         /* signature data */
    //struct stat      sb;
    UPDATE          *upd = &g_upd;
    LNODEID         *ln;


    /* options / operating mode variables */
    int     erase;       /* 1=erase chip, 0=don't */
    //  int     calibrate;   /* 1=calibrate RC oscillator, 0=don't */
    char   *port;        /* device port (/dev/xxx) */
    //int     terminal;    /* 1=enter terminal mode, 0=don't */
    //int     verify;      /* perform a verify operation */
    //char  * exitspecs;   /* exit specs string from command line */
    //char   *programmer;  /* programmer id */
    //char   *partdesc;    /* part id */
    //char    sys_config[PATH_MAX]; /* system wide config file */
    //char    usr_config[PATH_MAX]; /* per-user config file */
    //char  * e;           /* for strtol() error checking */
    int32     baudrate;    /* override default programmer baud rate */
    double  bitclock;    /* Specify programmer bit clock (JTAG ICE) */
    int     ispdelay;    /* Specify the delay for ISP clock */
    int     safemode;    /* Enable safemode, 1=safemode on, 0=normal */
    int     silentsafe;  /* Don't ask about fuses, 1=silent, 0=normal */
    int     init_ok = 0;     /* Device initialization worked well */
    int     is_open;     /* Device open succeeded */
    //char  * logfile;     /* Use logfile rather than stderr for diagnostics */
    enum updateflags uflags = UF_AUTO_ERASE; /* Flags for do_op() */
    unsigned char safemode_lfuse = 0xff;
    unsigned char safemode_hfuse = 0xff;
    unsigned char safemode_efuse = 0xff;
    unsigned char safemode_fuse = 0xff;

    char *safemode_response;
    int fuses_specified = 0;
    int fuses_updated = 0;
#if !defined(WIN32NATIVE)
    char   *homedir;
#endif
    //uint16 usedmem = osal_heap_mem_used();
    
    /*
     * Set line buffering for file descriptors so we see stdout and stderr
     * properly interleaved.
     */
    //setvbuf(stdout, (char*)NULL, _IOLBF, 0);
    //setvbuf(stderr, (char*)NULL, _IOLBF, 0);

    //progname = strrchr(argv[0],'/');
    progname = "avrdude";
#if defined (WIN32NATIVE)
    /* take care of backslash as dir sep in W32 */
    if (!progname) progname = strrchr(argv[0], '\\');
#endif /* WIN32NATIVE */
#if 0
    if (progname)
        progname++;
    else
        progname = argv[0];

    default_parallel[0] = 0;
    default_serial[0]   = 0;
    default_bitclock    = 0.0;
    default_safemode    = -1;

    init_config();

    atexit(cleanup_main);
#endif
    //updates = lcreat(NULL, 0);
    SerialPrintString("...000\r\n");
#if 0
    if (updates == NULL)
    {
        fprintf(stderr, "%s: cannot initialize updater list\n", progname);
        exit(1);
    }

    extended_params = lcreat(NULL, 0);
    if (extended_params == NULL)
    {
        fprintf(stderr, "%s: cannot initialize extended parameter list\n", progname);
        exit(1);
    }

    additional_config_files = lcreat(NULL, 0);
    if (additional_config_files == NULL)
    {
        fprintf(stderr, "%s: cannot initialize additional config files list\n", progname);
        exit(1);
    }
#endif
    //partdesc      = NULL;
    port          = NULL;
    erase         = 0;
    //calibrate     = 0;
    p             = NULL;
    ovsigck       = 0;
    //terminal      = 0;
    //verify        = 1;        /* on by default */
    //quell_progress = 0;
    //exitspecs     = NULL;
    pgm           = NULL;
    //programmer    = default_programmer;
    //verbose       = 0;
    baudrate      = 0;
    bitclock      = 0.0;
    ispdelay      = 0;
    safemode      = 1;       /* Safemode on by default */
    silentsafe    = 0;       /* Ask by default */
    is_open       = 0;
    //logfile       = NULL;
#if 0
#if defined(WIN32NATIVE)

    win_sys_config_set(sys_config);
    win_usr_config_set(usr_config);

#else

    strcpy(sys_config, CONFIG_DIR);
    i = strlen(sys_config);
    if (i && (sys_config[i - 1] != '/'))
        strcat(sys_config, "/");
    strcat(sys_config, "avrdude.conf");

    usr_config[0] = 0;
    homedir = getenv("HOME");
    if (homedir != NULL)
    {
        strcpy(usr_config, homedir);
        i = strlen(usr_config);
        if (i && (usr_config[i - 1] != '/'))
            strcat(usr_config, "/");
        strcat(usr_config, ".avrduderc");
    }

#endif

    len = strlen(progname) + 2;
    for (i = 0; i < len; i++)
        progbuf[i] = ' ';
    progbuf[i] = 0;

    /*
     * check for no arguments
     */
    if (argc == 1)
    {
        usage();
        return 0;
    }
#endif

    port = "HAL_UART_PORT_0";
    //-p
    //partdesc = "atmega328p";

    //-c
    //programmer = "arduino";

    //-b
    baudrate = 115200;

    //-D
    uflags &= ~UF_AUTO_ERASE;/* disable auto erase */

    //-U
    char myoptarg[22] = {0};
    //strcpy(myoptarg, "flash:w:C:\\Users\\Blink.hex:i");
    strcpy(myoptarg, "flash:w:Blink.hex:i");
    parse_op_elara(upd);
    //upd = (UPDATE *)osal_mem_alloc(sizeof(UPDATE));
    if (upd == NULL)
    {
        //      fprintf(stderr, "%s: error parsing update operation '%s'\n", progname, optarg);
        //      exit(1);
    }
    
    //ladd(updates, upd);
    
#if 0
    if (verify && upd->op == DEVICE_WRITE)
    {
        upd = dup_update(upd);
        upd->op = DEVICE_VERIFY;
        ladd(updates, upd);
    }
#endif
#if 0
    /*
     * process command line arguments
     */
    while ((ch = getopt(argc, argv, "?b:B:c:C:DeE:Fi:l:np:OP:qstU:uvVx:yY:")) != -1)
    {

        switch (ch)
        {
        case 'b': /* override default programmer baud rate */
            baudrate = strtol(optarg, &e, 0);
            if ((e == optarg) || (*e != 0))
            {
                fprintf(stderr, "%s: invalid baud rate specified '%s'\n",
                        progname, optarg);
                exit(1);
            }
            break;

        case 'B':	/* specify JTAG ICE bit clock period */
            bitclock = strtod(optarg, &e);
            if ((e == optarg) || (*e != 0) || bitclock == 0.0)
            {
                fprintf(stderr, "%s: invalid bit clock period specified '%s'\n",
                        progname, optarg);
                exit(1);
            }
            break;

        case 'i':	/* specify isp clock delay */
            ispdelay = strtol(optarg, &e, 10);
            if ((e == optarg) || (*e != 0) || ispdelay == 0)
            {
                fprintf(stderr, "%s: invalid isp clock delay specified '%s'\n",
                        progname, optarg);
                exit(1);
            }
            break;

        case 'c': /* programmer id */
            programmer = optarg;
            break;

        case 'C': /* system wide configuration file */
            if (optarg[0] == '+')
            {
                ladd(additional_config_files, optarg + 1);
            }
            else
            {
                strncpy(sys_config, optarg, PATH_MAX);
                sys_config[PATH_MAX - 1] = 0;
            }
            break;

        case 'D': /* disable auto erase */
            uflags &= ~UF_AUTO_ERASE;
            break;

        case 'e': /* perform a chip erase */
            erase = 1;
            uflags &= ~UF_AUTO_ERASE;
            break;

        case 'E':
            exitspecs = optarg;
            break;

        case 'F': /* override invalid signature check */
            ovsigck = 1;
            break;

        case 'l':
            logfile = optarg;
            break;

        case 'n':
            uflags |= UF_NOWRITE;
            break;

        case 'O': /* perform RC oscillator calibration */
            calibrate = 1;
            break;

        case 'p' : /* specify AVR part */
            partdesc = optarg;
            break;

        case 'P':
            port = optarg;
            break;

        case 'q' : /* Quell progress output */
            quell_progress++ ;
            break;

        case 's' : /* Silent safemode */
            silentsafe = 1;
            safemode = 1;
            break;

        case 't': /* enter terminal mode */
            terminal = 1;
            break;

        case 'u' : /* Disable safemode */
            safemode = 0;
            break;

        case 'U':
            upd = parse_op(optarg);
            if (upd == NULL)
            {
                fprintf(stderr, "%s: error parsing update operation '%s'\n",
                        progname, optarg);
                exit(1);
            }
            ladd(updates, upd);

            if (verify && upd->op == DEVICE_WRITE)
            {
                upd = dup_update(upd);
                upd->op = DEVICE_VERIFY;
                ladd(updates, upd);
            }
            break;

        case 'v':
            verbose++;
            break;

        case 'V':
            verify = 0;
            break;

        case 'x':
            ladd(extended_params, optarg);
            break;

        case 'y':
            fprintf(stderr, "%s: erase cycle counter no longer supported\n",
                    progname);
            break;

        case 'Y':
            fprintf(stderr, "%s: erase cycle counter no longer supported\n",
                    progname);
            break;

        case '?': /* help */
            usage();
            exit(0);
            break;

        default:
            fprintf(stderr, "%s: invalid option -%c\n\n", progname, ch);
            usage();
            exit(1);
            break;
        }

    }

    if (logfile != NULL)
    {
        FILE *newstderr = freopen(logfile, "w", stderr);
        if (newstderr == NULL)
        {
            /* Help!  There's no stderr to complain to anymore now. */
            printf("Cannot create logfile \"%s\": %s\n",
                   logfile, strerror(errno));
            return 1;
        }
    }

    if (quell_progress == 0)
    {
        if (isatty (STDERR_FILENO))
            update_progress = update_progress_tty;
        else
        {
            update_progress = update_progress_no_tty;
            /* disable all buffering of stderr for compatibility with
               software that captures and redirects output to a GUI
               i.e. Programmers Notepad */
            setvbuf( stderr, NULL, _IONBF, 0 );
            setvbuf( stdout, NULL, _IONBF, 0 );
        }
    }

    if (verbose)
    {
        /*
         * Print out an identifying string so folks can tell what version
         * they are running
         */
        fprintf(stderr,
                "\n%s: Version %s, compiled on %s at %s\n"
                "%sCopyright (c) 2000-2005 Brian Dean, http://www.bdmicro.com/\n"
                "%sCopyright (c) 2007-2014 Joerg Wunsch\n\n",
                progname, version, __DATE__, __TIME__, progbuf, progbuf);
    }

    if (verbose)
    {
        fprintf(stderr, "%sSystem wide configuration file is \"%s\"\n",
                progbuf, sys_config);
    }

    rc = read_config(sys_config);
    if (rc)
    {
        fprintf(stderr,
                "%s: error reading system wide configuration file \"%s\"\n",
                progname, sys_config);
        exit(1);
    }

    if (usr_config[0] != 0)
    {
        if (verbose)
        {
            fprintf(stderr, "%sUser configuration file is \"%s\"\n",
                    progbuf, usr_config);
        }

        rc = stat(usr_config, &sb);
        if ((rc < 0) || ((sb.st_mode & S_IFREG) == 0))
        {
            if (verbose)
            {
                fprintf(stderr,
                        "%sUser configuration file does not exist or is not a "
                        "regular file, skipping\n",
                        progbuf);
            }
        }
        else
        {
            rc = read_config(usr_config);
            if (rc)
            {
                fprintf(stderr, "%s: error reading user configuration file \"%s\"\n",
                        progname, usr_config);
                exit(1);
            }
        }
    }

    if (lsize(additional_config_files) > 0)
    {
        LNODEID ln1;
        const char *p = NULL;

        for (ln1 = lfirst(additional_config_files); ln1; ln1 = lnext(ln1))
        {
            p = ldata(ln1);
            if (verbose)
            {
                fprintf(stderr, "%sAdditional configuration file is \"%s\"\n",
                        progbuf, p);
            }

            rc = read_config(p);
            if (rc)
            {
                fprintf(stderr,
                        "%s: error reading additional configuration file \"%s\"\n",
                        progname, p);
                exit(1);
            }
        }
    }
#endif
#if 0
    // set bitclock from configuration files unless changed by command line
    if (default_bitclock > 0 && bitclock == 0.0)
    {
        bitclock = default_bitclock;
    }

    if (verbose)
    {
        fprintf(stderr, "\n");
    }
#endif
#if 0
    if (partdesc)
    {
        if (strcmp(partdesc, "?") == 0)
        {
            fprintf(stderr, "\n");
            fprintf(stderr, "Valid parts are:\n");
            list_parts(stderr, "  ", part_list);
            fprintf(stderr, "\n");
            exit(1);
        }
    }

    if (programmer)
    {
        if (strcmp(programmer, "?") == 0)
        {
            fprintf(stderr, "\n");
            fprintf(stderr, "Valid programmers are:\n");
            list_programmers(stderr, "  ", programmers);
            fprintf(stderr, "\n");
            exit(1);
        }
        if (strcmp(programmer, "?type") == 0)
        {
            fprintf(stderr, "\n");
            fprintf(stderr, "Valid programmer types are:\n");
            list_programmer_types(stderr, "  ");
            fprintf(stderr, "\n");
            exit(1);
        }
    }


    if (programmer[0] == 0)
    {
        fprintf(stderr,
                "\n%s: no programmer has been specified on the command line "
                "or the config file\n",
                progname);
        fprintf(stderr,
                "%sSpecify a programmer using the -c option and try again\n\n",
                progbuf);
        exit(1);
    }
#endif
#if 0
    pgm = locate_programmer(programmers, programmer);
    if (pgm == NULL)
    {
        fprintf(stderr, "\n");
        fprintf(stderr,
                "%s: Can't find programmer id \"%s\"\n",
                progname, programmer);
        fprintf(stderr, "\nValid programmers are:\n");
        list_programmers(stderr, "  ", programmers);
        fprintf(stderr, "\n");
        exit(1);
    }
#endif
    pgm = &ggm;
    init_programmer(pgm);
    SerialPrintString("333............\r\n");
    pgm->initpgm = &arduino_initpgm;
    if (pgm->initpgm)
    {
        pgm->initpgm(pgm);
    }
    else
    {
        //fprintf(stderr,
        //        "\n%s: Can't initialize the programmer.\n\n",
        //        progname);
        //exit(1);
    }
    SerialPrintString("444............\r\n");
    if (pgm->setup)
    {
        pgm->setup(pgm);
    }
    SerialPrintString("555............\r\n");
#if 0
    if (pgm->teardown)
    {
        atexit(exithook);
    }

    if (lsize(extended_params) > 0)
    {
        if (pgm->parseextparams == NULL)
        {
            fprintf(stderr,
                    "%s: WARNING: Programmer doesn't support extended parameters,"
                    " -x option(s) ignored\n",
                    progname);
        }
        else
        {
            if (pgm->parseextparams(pgm, extended_params) < 0)
            {
                fprintf(stderr,
                        "%s: Error parsing extended parameter list\n",
                        progname);
                exit(1);
            }
        }
    }

    if (port == NULL)
    {
        switch (pgm->conntype)
        {
        case CONNTYPE_PARALLEL:
            port = default_parallel;
            break;

        case CONNTYPE_SERIAL:
            port = default_serial;
            break;

        case CONNTYPE_USB:
            port = DEFAULT_USB;
            break;
        }
    }

    if (partdesc == NULL)
    {
        fprintf(stderr,
                "%s: No AVR part has been specified, use \"-p Part\"\n\n",
                progname);
        fprintf(stderr, "Valid parts are:\n");
        list_parts(stderr, "  ", part_list);
        fprintf(stderr, "\n");
        exit(1);
    }
#endif
#if 0
    p = locate_part(part_list, partdesc);
    if (p == NULL)
    {
        fprintf(stderr,
                "%s: AVR Part \"%s\" not found.\n\n",
                progname, partdesc);
        fprintf(stderr, "Valid parts are:\n");
        list_parts(stderr, "  ", part_list);
        fprintf(stderr, "\n");
        exit(1);
    }
#else
    
    memset(&gAvrPart, 0, sizeof(AVRPART));
    p = &gAvrPart;
    SerialPrintString("666............\r\n");
    initTheAvrPart_328p(p);
#endif
#if 0
    if (exitspecs != NULL)
    {
        if (pgm->parseexitspecs == NULL)
        {
            fprintf(stderr,
                    "%s: WARNING: -E option not supported by this programmer type\n",
                    progname);
            exitspecs = NULL;
        }
        else if (pgm->parseexitspecs(pgm, exitspecs) < 0)
        {
            usage();
            exit(1);
        }
    }
#endif
#if 0
    if (default_safemode == 0)
    {
        /* configuration disables safemode: revert meaning of -u */
        if (safemode == 0)
            /* -u was given: enable safemode */
            safemode = 1;
        else
            /* -u not given: turn off */
            safemode = 0;
    }

    if (isatty(STDIN_FILENO) == 0 && silentsafe == 0)
        safemode  = 0;       /* Turn off safemode if this isn't a terminal */


    if(p->flags & AVRPART_AVR32)
    {
        safemode = 0;
    }

    if(p->flags & (AVRPART_HAS_PDI | AVRPART_HAS_TPI))
    {
        safemode = 0;
    }
#endif
    safemode = 0;

    if (avr_initmem(p) != 0)
    {
        //fprintf(stderr, "\n%s: failed to initialize memories\n",
        //        progname);
        //exit(1);
    }
#if 0
    /*
     * Now that we know which part we are going to program, locate any
     * -U options using the default memory region, and fill in the
     * device-dependent default region name, either "application" (for
     * Xmega devices), or "flash" (everything else).
     */
    for (ln = lfirst(updates); ln; ln = lnext(ln))
    {
        upd = ldata(ln);

        if (upd->memtype == NULL)
        {
            const char *mtype = (p->flags & AVRPART_HAS_PDI) ? "application" : "flash";
            if (verbose >= 2)
            {
                fprintf(stderr,
                        "%s: defaulting memtype in -U %c:%s option to \"%s\"\n",
                        progname,
                        (upd->op == DEVICE_READ) ? 'r' : (upd->op == DEVICE_WRITE) ? 'w' : 'v',
                        upd->filename, mtype);
            }
            if ((upd->memtype = strdup(mtype)) == NULL)
            {
                fprintf(stderr, "%s: out of memory\n", progname);
                exit(1);
            }
        }
    }
#endif
    
#if 0
    /*
     * open the programmer
     */
    if (port[0] == 0)
    {
        fprintf(stderr, "\n%s: no port has been specified on the command line "
                "or the config file\n",
                progname);
        fprintf(stderr, "%sSpecify a port using the -P option and try again\n\n",
                progbuf);
        exit(1);
    }

    if (verbose)
    {
        fprintf(stderr, "%sUsing Port                    : %s\n", progbuf, port);
        fprintf(stderr, "%sUsing Programmer              : %s\n", progbuf, programmer);
        if ((strcmp(pgm->type, "avr910") == 0))
        {
            fprintf(stderr, "%savr910_devcode (avrdude.conf) : ", progbuf);
            if(p->avr910_devcode)fprintf(stderr, "0x%x\n", p->avr910_devcode);
            else fprintf(stderr, "none\n");
        }
    }
#endif
    if (baudrate != 0)
    {
        //if (verbose) {
        //  fprintf(stderr, "%sOverriding Baud Rate          : %d\n", progbuf, baudrate);
        //}
        pgm->baudrate = baudrate;
    }

    if (bitclock != 0.0)
    {
        //if (verbose)
        {
            //fprintf(stderr, "%sSetting bit clk period        : %.1f\n", progbuf, bitclock);
        }
        pgm->bitclock = bitclock * 1e-6;
    }

    if (ispdelay != 0)
    {
        //if (verbose)
        {
            //fprintf(stderr, "%sSetting isp clock delay        : %3i\n", progbuf, ispdelay);
        }
        pgm->ispdelay = ispdelay;
    }

    rc = pgm->open(pgm, port);
    SerialPrintString("10............\r\n");
    if (rc < 0)
    {
        SerialPrintString("11............\r\n");
        exitrc = rc;
        pgm->ppidata = 0; /* clear all bits at exit */
        goto main_exit;
    }
    SerialPrintString("12............\r\n");
    is_open = 1;
#if 0
    if (calibrate)
    {
        /*
         * perform an RC oscillator calibration
         * as outlined in appnote AVR053
         */
        if (pgm->perform_osccal == 0)
        {
            //fprintf(stderr,
            //        "%s: programmer does not support RC oscillator calibration\n",
            //    progname);
            exitrc = 1;
        }
        else
        {
            //fprintf(stderr, "%s: performing RC oscillator calibration\n", progname);
            exitrc = pgm->perform_osccal(pgm);
        }
        if (exitrc == 0 && quell_progress < 2)
        {
            //fprintf(stderr,
            //        "%s: calibration value is now stored in EEPROM at address 0\n",
            //        progname);
        }
        goto main_exit;
    }
#endif
#if 0
    if (verbose)
    {
        avr_display(stderr, p, progbuf, verbose);
        fprintf(stderr, "\n");
        programmer_display(pgm, progbuf);
    }

    if (quell_progress < 2)
    {
        fprintf(stderr, "\n");
    }
#endif
    exitrc = 0;

    /*
     * enable the programmer
     */
    pgm->enable(pgm);

    /*
     * turn off all the status leds
     */
    pgm->rdy_led(pgm, OFF);
    pgm->err_led(pgm, OFF);
    pgm->pgm_led(pgm, OFF);
    pgm->vfy_led(pgm, OFF);

    /*
     * initialize the chip in preperation for accepting commands
     */
    init_ok = (rc = pgm->initialize(pgm, p)) >= 0;
    //return -rc;
    SerialPrintString("38............\r\n");
    if (!init_ok)
    {
        SerialPrintString("39............\r\n");
        //fprintf(stderr, "%s: initialization failed, rc=%d\n", progname, rc);
        #if 1
        if (!ovsigck)
        {
            SerialPrintString("40............\r\n");
            //fprintf(stderr, "%sDouble check connections and try again, "
            //        "or use -F to override\n"
            //        "%sthis check.\n\n",
            //        progbuf, progbuf);
            //exitrc = 1;
            exitrc = rc;
            goto main_exit;
        }
        #endif
    }
    SerialPrintString("41............\r\n");
    /* indicate ready */
    pgm->rdy_led(pgm, ON);
#if 0
    if (quell_progress < 2)
    {
        fprintf(stderr,
                "%s: AVR device initialized and ready to accept instructions\n",
                progname);
    }
#endif
    /*
     * Let's read the signature bytes to make sure there is at least a
     * chip on the other end that is responding correctly.  A check
     * against 0xffffff / 0x000000 should ensure that the signature bytes
     * are valid.
     */
#if 0 // lxy added.
    if(!(p->flags & AVRPART_AVR32))
    {
        int attempt = 0;
        int waittime = 10000;       /* 10 ms */

sig_again:
        delay_nus(waittime);
        if (init_ok)
        {
            rc = avr_signature(pgm, p);
            if (rc != 0)
            {
                //fprintf(stderr, "%s: error reading signature data, rc=%d\n",
                //  progname, rc);
                exitrc = 1;
                goto main_exit;
            }
        }

        sig = avr_locate_mem(p, "signature");
        if (sig == NULL)
        {
            //fprintf(stderr,
            //        "%s: WARNING: signature data not defined for device \"%s\"\n",
            //        progname, p->desc);
        }

        if (sig != NULL)
        {
            int ff, zz;
#if 0
            if (quell_progress < 2)
            {
                fprintf(stderr, "%s: Device signature = 0x", progname);
            }
#endif
            ff = zz = 1;
            for (i = 0; i < sig->size; i++)
            {
#if 0
                if (quell_progress < 2)
                {
                    fprintf(stderr, "%02x", sig->buf[i]);
                }
#endif
                if (sig->buf[i] != 0xff)
                    ff = 0;
                if (sig->buf[i] != 0x00)
                    zz = 0;
            }
            if (ff || zz)
            {
                if (++attempt < 3)
                {
                    waittime *= 5;
#if 0
                    if (quell_progress < 2)
                    {
                        fprintf(stderr, " (retrying)\n");
                    }
#endif
                    goto sig_again;
                }
#if 0
                if (quell_progress < 2)
                {
                    fprintf(stderr, "\n");
                }
                fprintf(stderr,
                        "%s: Yikes!  Invalid device signature.\n", progname);
#endif
                if (!ovsigck)
                {
                    //fprintf(stderr, "%sDouble check connections and try again, "
                    //        "or use -F to override\n"
                    //        "%sthis check.\n\n",
                    //        progbuf, progbuf);
                    exitrc = 1;
                    goto main_exit;
                }
            }
            else
            {
                //if (quell_progress < 2) {
                //  fprintf(stderr, "\n");
                //}
            }

            if (sig->size != 3 ||
                    sig->buf[0] != p->signature[0] ||
                    sig->buf[1] != p->signature[1] ||
                    sig->buf[2] != p->signature[2])
            {
                //fprintf(stderr,
                //        "%s: Expected signature for %s is %02X %02X %02X\n",
                //        progname, p->desc,
                //        p->signature[0], p->signature[1], p->signature[2]);
                if (!ovsigck)
                {
                    //fprintf(stderr, "%sDouble check chip, "
                    //        "or use -F to override this check.\n",
                    //        progbuf);
                    exitrc = 1;
                    goto main_exit;
                }
            }
        }
    }
#endif
    
#if 0
    if (init_ok && safemode == 1)
    {
        /* If safemode is enabled, go ahead and read the current low, high,
           and extended fuse bytes as needed */

        rc = safemode_readfuses(&safemode_lfuse, &safemode_hfuse,
                                &safemode_efuse, &safemode_fuse, pgm, p, verbose);

        if (rc != 0)
        {

            //Check if the programmer just doesn't support reading
            if (rc == -5)
            {
                if (verbose > 0)
                {
                    fprintf(stderr, "%s: safemode: Fuse reading not support by programmer.\n"
                            "              Safemode disabled.\n", progname);
                }
                safemode = 0;
            }
            else
            {

                fprintf(stderr, "%s: safemode: To protect your AVR the programming "
                        "will be aborted\n",
                        progname);
                exitrc = 1;
                goto main_exit;
            }
        }
        else
        {
            //Save the fuses as default
            safemode_memfuses(1, &safemode_lfuse, &safemode_hfuse, &safemode_efuse, &safemode_fuse);
        }
    }

    if (uflags & UF_AUTO_ERASE)
    {
        if ((p->flags & AVRPART_HAS_PDI) && pgm->page_erase != NULL &&
                lsize(updates) > 0)
        {
            if (quell_progress < 2)
            {
                //fprintf(stderr,
                //        "%s: NOTE: Programmer supports page erase for Xmega devices.\n"
                //        "%sEach page will be erased before programming it, but no chip erase is performed.\n"
                //        "%sTo disable page erases, specify the -D option; for a chip-erase, use the -e option.\n",
                //        progname, progbuf, progbuf);
            }
        }
        else
        {
            AVRMEM *m;
            const char *memname = (p->flags & AVRPART_HAS_PDI) ? "application" : "flash";

            uflags &= ~UF_AUTO_ERASE;
            for (ln = lfirst(updates); ln; ln = lnext(ln))
            {
                upd = ldata(ln);
                m = avr_locate_mem(p, upd->memtype);
                if (m == NULL)
                    continue;
                if ((strcasecmp(m->desc, memname) == 0) && (upd->op == DEVICE_WRITE))
                {
                    erase = 1;
                    if (quell_progress < 2)
                    {
                        //fprintf(stderr,
                        //        "%s: NOTE: \"%s\" memory has been specified, an erase cycle "
                        //        "will be performed\n"
                        //        "%sTo disable this feature, specify the -D option.\n",
                        //        progname, memname, progbuf);
                    }
                    break;
                }
            }
        }
    }
#endif
    if (init_ok && erase)
    {
        SerialPrintString("42............\r\n");
        /*
         * erase the chip's flash and eeprom memories, this is required
         * before the chip can accept new programming
         */
        if (uflags & UF_NOWRITE)
        {
            SerialPrintString("43............\r\n");
            //fprintf(stderr,
            //    "%s: conflicting -e and -n options specified, NOT erasing chip\n",
            //    progname);
        }
        else
        {
            SerialPrintString("44............\r\n");
            //if (quell_progress < 2)
            {
                //fprintf(stderr, "%s: erasing chip\n", progname);
            }
            exitrc = avr_chip_erase(pgm, p);
            if(exitrc) goto main_exit;
        }
    }
#if 0
    if (terminal)
    {
        /*
         * terminal mode
         */
        exitrc = terminal_mode(pgm, p);
    }
#endif
    SerialPrintString("45............\r\n");
    if (!init_ok)
    {
        SerialPrintString("46............\r\n");
        /*
         * If we came here by the -tF options, bail out now.
         */
        exitrc = 1;
        goto main_exit;
    }
    SerialPrintString("47............\r\n");
#if 0
    for (ln = lfirst(updates); ln; ln = lnext(ln))
    {
        upd = ldata(ln);
        rc = do_op(pgm, p, upd, uflags);
        if (rc)
        {
            exitrc = 1;
            break;
        }
    }
#else
    rc = do_op(pgm, p, upd, uflags);
    //return -rc;
    exitrc = rc;
    SerialPrintString("48............\r\n");
#endif
    
#if 0
    /* Right before we exit programming mode, which will make the fuse
       bits active, check to make sure they are still correct */
    if (safemode == 1)
    {
        /* If safemode is enabled, go ahead and read the current low,
         * high, and extended fuse bytes as needed */
        unsigned char safemodeafter_lfuse = 0xff;
        unsigned char safemodeafter_hfuse = 0xff;
        unsigned char safemodeafter_efuse = 0xff;
        unsigned char safemodeafter_fuse  = 0xff;
        unsigned char failures = 0;
        char yes[1] = {'y'};

        if (quell_progress < 2)
        {
            //fprintf(stderr, "\n");
        }

        //Restore the default fuse values
        safemode_memfuses(0, &safemode_lfuse, &safemode_hfuse, &safemode_efuse, &safemode_fuse);

        /* Try reading back fuses, make sure they are reliable to read back */
        if (safemode_readfuses(&safemodeafter_lfuse, &safemodeafter_hfuse,
                               &safemodeafter_efuse, &safemodeafter_fuse, pgm, p, verbose) != 0)
        {
            /* Uh-oh.. try once more to read back fuses */
            if (safemode_readfuses(&safemodeafter_lfuse, &safemodeafter_hfuse,
                                   &safemodeafter_efuse, &safemodeafter_fuse, pgm, p, verbose) != 0)
            {
                fprintf(stderr,
                        "%s: safemode: Sorry, reading back fuses was unreliable. "
                        "I have given up and exited programming mode\n",
                        progname);
                exitrc = 1;
                goto main_exit;
            }
        }

        /* Now check what fuses are against what they should be */
        if (safemodeafter_fuse != safemode_fuse)
        {
            fuses_updated = 1;
            fprintf(stderr, "%s: safemode: fuse changed! Was %x, and is now %x\n",
                    progname, safemode_fuse, safemodeafter_fuse);


            /* Ask user - should we change them */

            if (silentsafe == 0)
                safemode_response = terminal_get_input("Would you like this fuse to be changed back? [y/n] ");
            else
                safemode_response = yes;

            if (tolower((int)(safemode_response[0])) == 'y')
            {

                /* Enough chit-chat, time to program some fuses and check them */
                if (safemode_writefuse (safemode_fuse, "fuse", pgm, p,
                                        10, verbose) == 0)
                {
                    fprintf(stderr, "%s: safemode: and is now rescued\n", progname);
                }
                else
                {
                    fprintf(stderr, "%s: and COULD NOT be changed\n", progname);
                    failures++;
                }
            }
        }

        /* Now check what fuses are against what they should be */
        if (safemodeafter_lfuse != safemode_lfuse)
        {
            fuses_updated = 1;
            fprintf(stderr, "%s: safemode: lfuse changed! Was %x, and is now %x\n",
                    progname, safemode_lfuse, safemodeafter_lfuse);


            /* Ask user - should we change them */

            if (silentsafe == 0)
                safemode_response = terminal_get_input("Would you like this fuse to be changed back? [y/n] ");
            else
                safemode_response = yes;

            if (tolower((int)(safemode_response[0])) == 'y')
            {

                /* Enough chit-chat, time to program some fuses and check them */
                if (safemode_writefuse (safemode_lfuse, "lfuse", pgm, p,
                                        10, verbose) == 0)
                {
                    fprintf(stderr, "%s: safemode: and is now rescued\n", progname);
                }
                else
                {
                    fprintf(stderr, "%s: and COULD NOT be changed\n", progname);
                    failures++;
                }
            }
        }

        /* Now check what fuses are against what they should be */
        if (safemodeafter_hfuse != safemode_hfuse)
        {
            fuses_updated = 1;
            fprintf(stderr, "%s: safemode: hfuse changed! Was %x, and is now %x\n",
                    progname, safemode_hfuse, safemodeafter_hfuse);

            /* Ask user - should we change them */
            if (silentsafe == 0)
                safemode_response = terminal_get_input("Would you like this fuse to be changed back? [y/n] ");
            else
                safemode_response = yes;
            if (tolower((int)(safemode_response[0])) == 'y')
            {

                /* Enough chit-chat, time to program some fuses and check them */
                if (safemode_writefuse(safemode_hfuse, "hfuse", pgm, p,
                                       10, verbose) == 0)
                {
                    fprintf(stderr, "%s: safemode: and is now rescued\n", progname);
                }
                else
                {
                    fprintf(stderr, "%s: and COULD NOT be changed\n", progname);
                    failures++;
                }
            }
        }

        /* Now check what fuses are against what they should be */
        if (safemodeafter_efuse != safemode_efuse)
        {
            fuses_updated = 1;
            fprintf(stderr, "%s: safemode: efuse changed! Was %x, and is now %x\n",
                    progname, safemode_efuse, safemodeafter_efuse);

            /* Ask user - should we change them */
            if (silentsafe == 0)
                safemode_response = terminal_get_input("Would you like this fuse to be changed back? [y/n] ");
            else
                safemode_response = yes;
            if (tolower((int)(safemode_response[0])) == 'y')
            {

                /* Enough chit-chat, time to program some fuses and check them */
                if (safemode_writefuse (safemode_efuse, "efuse", pgm, p,
                                        10, verbose) == 0)
                {
                    fprintf(stderr, "%s: safemode: and is now rescued\n", progname);
                }
                else
                {
                    fprintf(stderr, "%s: and COULD NOT be changed\n", progname);
                    failures++;
                }
            }
        }

        if (quell_progress < 2)
        {
            fprintf(stderr, "%s: safemode: ", progname);
            if (failures == 0)
            {
                fprintf(stderr, "Fuses OK (E:%02X, H:%02X, L:%02X)\n",
                        safemode_efuse, safemode_hfuse, safemode_lfuse);
            }
            else
            {
                fprintf(stderr, "Fuses not recovered, sorry\n");
            }
        }

        if (fuses_updated && fuses_specified)
        {
            exitrc = 1;
        }

    }
#endif

main_exit:

    /*
     * program complete
     */

    if (is_open)
    {
        SerialPrintString("Leave avr_init()\r\n");
        pgm->powerdown(pgm);// Do nothing --- lxy added.

        rc = pgm->disable(pgm);
        if (rc < 0)
        {
            exitrc = rc;
        }

        pgm->rdy_led(pgm, OFF);

        pgm->close(pgm);
    }
    SerialPrintString("49............\r\n");
    //if (quell_progress < 2)
    {
        //fprintf(stderr, "\n%s done.  Thank you.\n\n", progname);
    }

    return exitrc;
    //return -48;
}

static int parse_cmdbits2(OPCODE *op, int bitno, char *ch)
{
    return -1;
    char aStr[3];
    char *q;
    char *e;
    aStr[0] = *ch;
    aStr[1] = *(ch + 1);
    aStr[2] = 0;

    if (aStr[0] == 'a')
    {
        q = &aStr[1];
        op->bit[bitno].bitno = strtol(q, &e, 0);
#if 0
        if ((e == q) || (*e != 0))
        {
            fprintf(stderr,
                    "%s: error at %s:%d: can't parse bit number from \"%s\"\n",
                    progname, infile, lineno, q);
            //exit(1);
            return -1;
        }
#endif
        op->bit[bitno].type = AVR_CMDBIT_ADDRESS;
        op->bit[bitno].value = 0;
    }
    return 0;
}

static int parse_cmdbits3(OPCODE *op, int bitno, char *ch)
{
    return -3;
    char aStr[4];
    char *q;
    char *e;
    aStr[0] = *ch;
    aStr[1] = *(ch + 1);
    aStr[2] = *(ch + 2);
    aStr[3] = 0;

    if (aStr[0] == 'a')
    {
        q = &aStr[1];
        op->bit[bitno].bitno = strtol(q, &e, 0);
#if 0
        if ((e == q) || (*e != 0))
        {
            fprintf(stderr,
                    "%s: error at %s:%d: can't parse bit number from \"%s\"\n",
                    progname, infile, lineno, q);
            //exit(1);
            return -1;
        }
#endif
        op->bit[bitno].type = AVR_CMDBIT_ADDRESS;
        op->bit[bitno].value = 0;
    }
    return 0;
}

static int parse_cmdbits(OPCODE *op, int bitno, char ch)
{
    return -1;
    switch (ch)
    {
    case '1':
        op->bit[bitno].type  = AVR_CMDBIT_VALUE;
        op->bit[bitno].value = 1;
        op->bit[bitno].bitno = bitno % 8;
        break;
    case '0':
        op->bit[bitno].type  = AVR_CMDBIT_VALUE;
        op->bit[bitno].value = 0;
        op->bit[bitno].bitno = bitno % 8;
        break;
    case 'x':
        op->bit[bitno].type  = AVR_CMDBIT_IGNORE;
        op->bit[bitno].value = 0;
        op->bit[bitno].bitno = bitno % 8;
        break;
    case 'a':
        op->bit[bitno].type  = AVR_CMDBIT_ADDRESS;
        op->bit[bitno].value = 0;
        op->bit[bitno].bitno = 8 * (bitno / 8) + bitno % 8;
        break;
    case 'i':
        op->bit[bitno].type  = AVR_CMDBIT_INPUT;
        op->bit[bitno].value = 0;
        op->bit[bitno].bitno = bitno % 8;
        break;
    case 'o':
        op->bit[bitno].type  = AVR_CMDBIT_OUTPUT;
        op->bit[bitno].value = 0;
        op->bit[bitno].bitno = bitno % 8;
        break;
    default :
        //fprintf(stderr,
        //        "%s: error at %s:%d: invalid bit specifier '%c'\n",
        //        progname, infile, lineno, ch);
        //exit(1);
        break;
    }
    return 0;
}

static unsigned char const g_eeprom_instr[EEPROM_INSTR_SIZE] = {0xBD, 0xF2, 0xBD, 0xE1, 0xBB, 0xCF, 0xB4, 0x00,
                                                         0xBE, 0x01, 0xB6, 0x01, 0xBC, 0x00, 0xBB, 0xBF,
                                                         0x99, 0xF9, 0xBB, 0xAF
                                                        };
static unsigned char const g_controlstack[CTL_STACK_SIZE] = {0x0E, 0x1E, 0x0F, 0x1F, 0x2E, 0x3E, 0x2F,
                                                      0x3F, 0x4E, 0x5E, 0x4F, 0x5F, 0x6E, 0x7E, 0x6F, 0x7F,
                                                      0x66, 0x76, 0x67, 0x77, 0x6A, 0x7A, 0x6B, 0x7B, 0xBE,
                                                      0xFD, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00
                                                     };
//OPCODE g_op[15];
//AVRMEM g_mem[8];
//OPCODE g_op[2];
static AVRMEM g_mem[8];
static LIST g_memList;
static const OPCODE g_op_eeprom_read = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
static const OPCODE g_op_eeprom_write = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,1,6},{1,1,7},
}};
static const OPCODE g_op_eeprom_LOADPAGE_LO = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {2,0,0},{2,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,1,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,1,6},{1,1,7},
}};
static const OPCODE g_op_eeprom_WRITEPAGE = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,1,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,1,6},{1,1,7},
}};
static uint8 g_eeprom_buf[128];
static uint8 g_eeprom_tags[128];
static void init_memtype_eeprom(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_eeprom()\r\n");

    current_mem->buf = g_eeprom_buf;
    current_mem->tags = g_eeprom_tags;
    
    strncpy(current_mem->desc, "eeprom", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->paged = 0;

    current_mem->page_size = 4;

    current_mem->size = 1024;

    current_mem->min_write_delay = 3600;

    current_mem->max_write_delay = 3600;

    current_mem->readback[0] = 0xff;
    current_mem->readback[1] = 0xff;

    current_mem->mode = 0x41;

    current_mem->delay = 20;

    current_mem->blocksize = 4;

    current_mem->readsize = 256;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_eeprom_read;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits2(op, bitno--, "a9");
    parse_cmdbits2(op, bitno--, "a8");

    parse_cmdbits2(op, bitno--, "a7");
    parse_cmdbits2(op, bitno--, "a6");
    parse_cmdbits2(op, bitno--, "a5");
    parse_cmdbits2(op, bitno--, "a4");
    parse_cmdbits2(op, bitno--, "a3");
    parse_cmdbits2(op, bitno--, "a2");
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;

    uint16 usedmem = 0;
#if defined(DEBUG_SERIAL)
    usedmem = osal_heap_mem_used();
    SerialPrintValue("Now BLE used memory is : ", usedmem, 10);
#endif
    //.........................................
    opnum = AVR_OP_WRITE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_eeprom_write;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
#if defined(DEBUG_SERIAL)
    usedmem = osal_heap_mem_used();
    SerialPrintValue("Now BLE used memory is : ", usedmem, 10);
#endif
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits2(op, bitno--, "a9");
    parse_cmdbits2(op, bitno--, "a8");

    parse_cmdbits2(op, bitno--, "a7");
    parse_cmdbits2(op, bitno--, "a6");
    parse_cmdbits2(op, bitno--, "a5");
    parse_cmdbits2(op, bitno--, "a4");
    parse_cmdbits2(op, bitno--, "a3");
    parse_cmdbits2(op, bitno--, "a2");
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');

    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_LOADPAGE_LO;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_eeprom_LOADPAGE_LO;
    SerialPrintValue("AVR_OP_LOADPAGE_LO op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');

    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITEPAGE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_eeprom_WRITEPAGE;
    SerialPrintValue("AVR_OP_WRITEPAGE op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits2(op, bitno--, "a9");
    parse_cmdbits2(op, bitno--, "a8");

    parse_cmdbits2(op, bitno--, "a7");
    parse_cmdbits2(op, bitno--, "a6");
    parse_cmdbits2(op, bitno--, "a5");
    parse_cmdbits2(op, bitno--, "a4");
    parse_cmdbits2(op, bitno--, "a3");
    parse_cmdbits2(op, bitno--, "a2");
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    current_mem->op[opnum] = op;
}

static const OPCODE g_op_flash_READ_LO = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{2,0,10},{2,0,11},{2,0,12},{2,0,13},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,1,5},{1,0,6},{1,0,7},
}};
static const OPCODE g_op_flash_READ_HI = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{2,0,10},{2,0,11},{2,0,12},{2,0,13},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,0,7},
}};
static const OPCODE g_op_flash_LOADPAGE_LO = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,1,6},{1,0,7},
}};
static const OPCODE g_op_flash_LOADPAGE_HI = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,0,5},{1,1,6},{1,0,7},
}};
static const OPCODE g_op_flash_WRITEPAGE = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{2,0,10},{2,0,11},{2,0,12},{2,0,13},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,0,5},{1,1,6},{1,0,7},
}};
static uint8 g_blink_bin[284] = {
    0x0c,0x94,0x34,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x11,0x24,0x1f,0xbe,0xcf,0xef,0xd8,0xe0,
    0xde,0xbf,0xcd,0xbf,0x0e,0x94,0x40,0x00,0x0c,0x94,0x8b,0x00,0x0c,0x94,0x00,0x00,
    0xcf,0x93,0xdf,0x93,0x00,0xd0,0x00,0xd0,0xcd,0xb7,0xde,0xb7,0x84,0xe2,0x90,0xe0,
    0x20,0xe2,0xfc,0x01,0x20,0x83,0x85,0xe2,0x90,0xe0,0x20,0xe2,0xfc,0x01,0x20,0x83,
    0x19,0x82,0x1a,0x82,0x1b,0x82,0x1c,0x82,0x0b,0xc0,0x89,0x81,0x9a,0x81,0xab,0x81,
    0xbc,0x81,0x01,0x96,0xa1,0x1d,0xb1,0x1d,0x89,0x83,0x9a,0x83,0xab,0x83,0xbc,0x83,
    0x89,0x81,0x9a,0x81,0xab,0x81,0xbc,0x81,0x80,0x3a,0xf6,0xe8,0x9f,0x07,0xf1,0xe0,
    0xaf,0x07,0xb1,0x05,0x54,0xf3,0x85,0xe2,0x90,0xe0,0xfc,0x01,0x10,0x82,0x19,0x82,
    0x1a,0x82,0x1b,0x82,0x1c,0x82,0x0b,0xc0,0x89,0x81,0x9a,0x81,0xab,0x81,0xbc,0x81,
    0x01,0x96,0xa1,0x1d,0xb1,0x1d,0x89,0x83,0x9a,0x83,0xab,0x83,0xbc,0x83,0x89,0x81,
    0x9a,0x81,0xab,0x81,0xbc,0x81,0x80,0x3a,0xf6,0xe8,0x9f,0x07,0xf1,0xe0,0xaf,0x07,
    0xb1,0x05,0x54,0xf3,0xc0,0xcf,0xf8,0x94,0xff,0xcf,
    0xff,0xff
};
static uint8 g_blink1_bin[292] = {
    0x0c,0x94,0x34,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,
    0x0c,0x94,0x3e,0x00,0x0c,0x94,0x3e,0x00,0x11,0x24,0x1f,0xbe,0xcf,0xef,0xd8,0xe0,
    0xde,0xbf,0xcd,0xbf,0x0e,0x94,0x40,0x00,0x0c,0x94,0x8f,0x00,0x0c,0x94,0x00,0x00,
    0xcf,0x93,0xdf,0x93,0xcd,0xb7,0xde,0xb7,0x28,0x97,0x0f,0xb6,0xf8,0x94,0xde,0xbf,
    0x0f,0xbe,0xcd,0xbf,0x84,0xe2,0x90,0xe0,0x20,0xe2,0xfc,0x01,0x20,0x83,0x85,0xe2,
    0x90,0xe0,0x20,0xe2,0xfc,0x01,0x20,0x83,0x19,0x82,0x1a,0x82,0x1b,0x82,0x1c,0x82,
    0x0b,0xc0,0x89,0x81,0x9a,0x81,0xab,0x81,0xbc,0x81,0x01,0x96,0xa1,0x1d,0xb1,0x1d,
    0x89,0x83,0x9a,0x83,0xab,0x83,0xbc,0x83,0x89,0x81,0x9a,0x81,0xab,0x81,0xfc,0x81,
    0x80,0x3a,0xf6,0xe8,0x9f,0x07,0xf1,0xe0,0xaf,0x07,0xb1,0x05,0x54,0xf3,0x85,0xe2,
    0x90,0xe0,0xfc,0x01,0x10,0x82,0x1d,0x82,0x1e,0x82,0x1f,0x82,0x18,0x86,0x0b,0xc0,
    0x8d,0x81,0x9e,0x81,0xaf,0x81,0xb8,0x85,0x01,0x96,0xa1,0x1d,0xb1,0x1d,0x8d,0x83,
    0x9e,0x83,0xaf,0x83,0xb8,0x87,0x8d,0x81,0x9e,0x81,0xaf,0x81,0xb8,0x85,0x80,0x34,
    0xf2,0xe4,0x9f,0x07,0xff,0xe0,0xaf,0x07,0xb1,0x05,0x54,0xf3,0xc0,0xcf,0xf8,0x94,
    0xff,0xcf,
    0xff,0xff,
};
static uint8 g_flash_buf[300];
static void init_memtype_flash(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_flash()\r\n");
    
//    current_mem->buf = (unsigned char*)osal_mem_alloc(128);
//    current_mem->tags = (unsigned char*)osal_mem_alloc(128);
    memset(g_flash_buf, 0, sizeof(g_flash_buf));
    //memcpy(g_flash_buf, (void*)g_blink_bin, sizeof(g_blink_bin));
    memcpy(g_flash_buf, (void*)g_blink1_bin, sizeof(g_blink1_bin));
    current_mem->buf = g_flash_buf;
    current_mem->tags = NULL;
    
    SerialPrintValue("buf = 0x",(uint16)current_mem->buf, 16);
    SerialPrintValue("tags = 0x",(uint16)current_mem->tags, 16);
    
    strncpy(current_mem->desc, "flash", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->paged = 1;

    current_mem->size = 32768;

    current_mem->page_size = 128;

    current_mem->num_pages = 256;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    current_mem->readback[0] = 0xff;
    current_mem->readback[1] = 0xff;

    current_mem->mode = 0x41;

    current_mem->delay = 6;

    current_mem->blocksize = 128;

    current_mem->readsize = 256;

    opnum = AVR_OP_READ_LO;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_flash_READ_LO;
    SerialPrintValue("AVR_OP_READ_LO op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits3(op, bitno--, "a13");
    parse_cmdbits3(op, bitno--, "a12");
    parse_cmdbits3(op, bitno--, "a11");
    parse_cmdbits3(op, bitno--, "a10");
    parse_cmdbits2(op, bitno--, "a9");
    parse_cmdbits2(op, bitno--, "a8");

    parse_cmdbits2(op, bitno--, "a7");
    parse_cmdbits2(op, bitno--, "a6");
    parse_cmdbits2(op, bitno--, "a5");
    parse_cmdbits2(op, bitno--, "a4");
    parse_cmdbits2(op, bitno--, "a3");
    parse_cmdbits2(op, bitno--, "a2");
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');

    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_READ_HI;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_flash_READ_HI;
    SerialPrintValue("AVR_OP_READ_HI op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits3(op, bitno--, "a13");
    parse_cmdbits3(op, bitno--, "a12");
    parse_cmdbits3(op, bitno--, "a11");
    parse_cmdbits3(op, bitno--, "a10");
    parse_cmdbits2(op, bitno--, "a9");
    parse_cmdbits2(op, bitno--, "a8");

    parse_cmdbits2(op, bitno--, "a7");
    parse_cmdbits2(op, bitno--, "a6");
    parse_cmdbits2(op, bitno--, "a5");
    parse_cmdbits2(op, bitno--, "a4");
    parse_cmdbits2(op, bitno--, "a3");
    parse_cmdbits2(op, bitno--, "a2");
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_LOADPAGE_LO;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_flash_LOADPAGE_LO;
    SerialPrintValue("AVR_OP_LOADPAGE_LO op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits2(op, bitno--, "a5");
    parse_cmdbits2(op, bitno--, "a4");
    parse_cmdbits2(op, bitno--, "a3");
    parse_cmdbits2(op, bitno--, "a2");
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_LOADPAGE_HI;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_flash_LOADPAGE_HI;
    SerialPrintValue("AVR_OP_LOADPAGE_HI op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits2(op, bitno--, "a5");
    parse_cmdbits2(op, bitno--, "a4");
    parse_cmdbits2(op, bitno--, "a3");
    parse_cmdbits2(op, bitno--, "a2");
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITEPAGE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_flash_WRITEPAGE;
    SerialPrintValue("AVR_OP_WRITEPAGE op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits3(op, bitno--, "a13");
    parse_cmdbits3(op, bitno--, "a12");
    parse_cmdbits3(op, bitno--, "a11");
    parse_cmdbits3(op, bitno--, "a10");
    parse_cmdbits2(op, bitno--, "a9");
    parse_cmdbits2(op, bitno--, "a8");

    parse_cmdbits2(op, bitno--, "a7");
    parse_cmdbits2(op, bitno--, "a6");
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    current_mem->op[opnum] = op;
}

static const OPCODE g_op_lfuse_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
}};
static const OPCODE g_op_lfuse_WRITE = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
static void init_memtype_lfuse(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_lfuse()\r\n");
    strncpy(current_mem->desc, "lfuse", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_lfuse_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_lfuse_WRITE;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    current_mem->op[opnum] = op;
}

static const OPCODE g_op_hfuse_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
}};
static const OPCODE g_op_hfuse_WRITE = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
static void init_memtype_hfuse(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_hfuse()\r\n");
    strncpy(current_mem->desc, "hfuse", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_hfuse_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_hfuse_WRITE;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    current_mem->op[opnum] = op;
}

static const OPCODE g_op_efuse_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
}};
static const OPCODE g_op_efuse_WRITE = {{
    {3,0,0},{3,0,1},{3,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,1,2},{1,0,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
static void init_memtype_efuse(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_efuse()\r\n");
    strncpy(current_mem->desc, "efuse", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_efuse_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_efuse_WRITE;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    current_mem->op[opnum] = op;
}

static const OPCODE g_op_lock_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
}};
static const OPCODE g_op_lock_WRITE = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{1,1,6},{1,1,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,1,5},{1,1,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
static void init_memtype_lock(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_lock()\r\n");
    strncpy(current_mem->desc, "lock", AVR_MEMDESCLEN);
    SerialPrintString(current_mem->desc);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_lock_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_lock_WRITE;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    parse_cmdbits(op, bitno--, 'i');
    current_mem->op[opnum] = op;
}

static const OPCODE g_op_calibration_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,1,4},{1,1,5},{1,0,6},{1,0,7},
}};
static void init_memtype_calibration(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_calibration()\r\n");
    strncpy(current_mem->desc, "calibration", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_calibration_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;
}

static const OPCODE g_op_signature_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {2,0,0},{2,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,1,4},{1,1,5},{1,0,6},{1,0,7},
}};
static void init_memtype_signature(AVRMEM *current_mem)
{
    int opnum;
    int bitno = 31;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_signature()\r\n");
    strncpy(current_mem->desc, "signature", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 3;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_signature_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits2(op, bitno--, "a1");
    parse_cmdbits2(op, bitno--, "a0");

    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    parse_cmdbits(op, bitno--, 'o');
    current_mem->op[opnum] = op;
}

static const OPCODE g_op_part_PGM_ENABLE = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,1,0},{1,1,1},{1,0,2},{1,0,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
static const OPCODE g_op_part_CHIP_ERASE = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
/*
const OPCODE g_op_ = {{
    {},{},{},{},{},{},{},{},
    {},{},{},{},{},{},{},{},
    {},{},{},{},{},{},{},{},
    {},{},{},{},{},{},{},{},
}};
*/
static void initTheAvrPart_328p(AVRPART *current_part)
{
    SerialPrintValue("Enter initTheAvrPart_328p() current_part = 0x",(uint16)current_part, 16);
    
    current_part->reset_disposition = RESET_DEDICATED;
    current_part->retry_pulse = PIN_AVR_SCK;
    current_part->flags = AVRPART_SERIALOK | AVRPART_ENABLEPAGEPROGRAMMING;
    memset(current_part->signature, 0xFF, 3);
    current_part->ctl_stack_type = CTL_STACK_NONE;
    current_part->ocdrev = -1;
    current_part->mem = lcreat(&g_memList, 8);
    SerialPrintValue("mem = 0x", (uint16)current_part->mem, 16);
    int nbytes = 0;
    strncpy(current_part->id, "m328p", AVR_IDLEN);
    current_part->id[AVR_IDLEN - 1] = 0;

    strncpy(current_part->desc, "ATmega328P", AVR_DESCLEN);
    current_part->desc[AVR_DESCLEN - 1] = 0;

    current_part->signature[0] = 0x1e;
    current_part->signature[1] = 0x95;
    current_part->signature[2] = 0x0F;

    current_part->ocdrev = 1;

    current_part->flags |= AVRPART_HAS_DW;

    current_part->flash_instr[nbytes++] = 0xB6;
    current_part->flash_instr[nbytes++] = 0x01;
    current_part->flash_instr[nbytes++] = 0x01;

    memcpy(current_part->eeprom_instr, (void *)g_eeprom_instr, EEPROM_INSTR_SIZE);

    current_part->stk500_devcode = 0x86;

    current_part->pagel = 0xd7;

    current_part->bs2 = 0xc2;

    current_part->chip_erase_delay = 9000;

    int opnum = AVR_OP_PGM_ENABLE;
    SerialPrintString("777............\r\n");
    //OPCODE *op = avr_new_opcode();
    OPCODE *op = (OPCODE*)&g_op_part_PGM_ENABLE;
    
    int bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    current_part->op[opnum] = op;
    ////////////////////////////////////////////////////////////////////////////
    opnum = AVR_OP_CHIP_ERASE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_part_CHIP_ERASE;
    SerialPrintValue("AVR_OP_CHIP_ERASE op = 0x",(uint16)op, 16);
    bitno = 31;
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');

    parse_cmdbits(op, bitno--, '1');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, '0');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');

    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    parse_cmdbits(op, bitno--, 'x');
    current_part->op[opnum] = op;

    current_part->timeout = 200;

    current_part->stabdelay = 100;

    current_part->cmdexedelay = 25;

    current_part->synchloops = 32;

    current_part->bytedelay = 0;

    current_part->pollindex = 3;

    current_part->pollvalue = 0x53;

    current_part->predelay = 1;

    current_part->postdelay = 1;

    current_part->pollmethod = 1;

    memcpy(current_part->controlstack, (void *)g_controlstack, CTL_STACK_SIZE);

    current_part->hventerstabdelay = 100;

    current_part->progmodedelay = 0;

    current_part->latchcycles = 5;

    current_part->togglevtg = 1;

    current_part->poweroffdelay = 15;

    current_part->resetdelayms = 1;

    current_part->resetdelayus = 0;

    current_part->hvleavestabdelay = 15;

    current_part->resetdelay = 15;

    current_part->chiperasepulsewidth = 0;

    current_part->chiperasepolltimeout = 10;

    current_part->programfusepulsewidth = 0;

    current_part->programfusepolltimeout = 5;

    current_part->programlockpulsewidth = 0;

    current_part->programlockpolltimeout = 5;

    /////////////////////////////////////////////////////////////////////
    AVRMEM *current_mem;
    //AVRMEM *current_mem = avr_new_memtype();
    current_mem = &g_mem[0];
    init_memtype_eeprom(current_mem);
    ladd(current_part->mem, current_mem);
    /////////////////////////////////////////////////////////////////////
#if 1
    SerialPrintString("888............\r\n");
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[1];
    init_memtype_flash(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[2];
    init_memtype_lfuse(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[3];
    init_memtype_hfuse(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[4];
    init_memtype_efuse(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[5];
    init_memtype_lock(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[6];
    init_memtype_calibration(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[7];
    init_memtype_signature(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
#endif
    SerialPrintString("9............\r\n");
}


int justForTestFlash(void)
{
    ELARA_LED1 = 1;
    #if 1
    elara_flash_write_init();
    uint16 page_size = 128;
    uint16 block_size;
    //uint32 binSize = 292;//sizeof(g_blink1_bin);
    uint32 binSize = 282;//sizeof(g_blink_bin);
    uint32 real = ((binSize + HAL_FLASH_WORD_SIZE - 1) / HAL_FLASH_WORD_SIZE) * HAL_FLASH_WORD_SIZE;//4 
    for (int i = 0; i < real; i+=block_size)
    {
        if (real - i < page_size)
        {
            block_size = real - i;
        }
        else
        {
            block_size = page_size;
        }
        elara_flash_write(block_size, g_blink_bin+i);
    }
    #endif
    //ELARA_LED1 = 0;
    return elara_flash_write_done();
    //return 0;
}

static void nothings(void)
{
}
