#ifndef MAIN_M644P_H
#define MAIN_M644P_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

#include "hal_types.h"

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */
extern unsigned char const g_controlstack_m644p[CTL_STACK_SIZE];
extern const OPCODE g_op_part_PGM_ENABLE_m644p;
extern const OPCODE g_op_part_CHIP_ERASE_m644p;
extern const OPCODE g_op_flash_READ_LO_m644p;
extern const OPCODE g_op_flash_READ_HI_m644p;
extern const OPCODE g_op_flash_LOADPAGE_LO_m644p;
extern const OPCODE g_op_flash_LOADPAGE_HI_m644p;
extern const OPCODE g_op_flash_WRITEPAGE_m644p;
/*********************************************************************
 * FUNCTIONS
 */
extern void initAvrPart_m644p(AVRPART *current_part, char* partId);
/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif
