#ifndef ELARA_FILE_COMMON_H
#define ELARA_FILE_COMMON_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

#include "hal_types.h"
#include "osal_snv.h"
#include "elara_file_spi_w25q_new.h"
/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * FUNCTIONS
 */
extern int elara_flash_common_init(uint8 flag);

extern int elara_flash_read_init(void);
extern void elara_flash_write_init(void);
extern void elara_flash_read( uint16 len, uint8 *pBuf );
extern uint32 elara_flash_if_flash_enough(uint32 binSize);
extern int elara_flash_write(uint16 len, uint8 *pBuf);
extern int elara_flash_write_done(void);
extern int elara_flash_erase(void);
extern void elara_flash_lost_connected(void);

extern uint32 elara_flash_any_thing_else(void);
extern bool elara_flash_if_erase_finished(void);
extern bool elara_flash_if_first_256_FF(void);
/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif
