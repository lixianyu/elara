/*********************************************************************
 * INCLUDES
 */
#include "bcomdef.h"
#include "hal_adc.h"
#include "hal_flash.h"
#include "hal_types.h"
#include "comdef.h"
#include "OSAL.h"
#include "OSAL_PwrMgr.h"
#include "osal_snv.h"
#include "hal_assert.h"
#include "saddr.h"
#include "elara_error.h"
#include "elara_file_cc2541.h"
#include "elara_file_spi_w25q_new.h"
#include "elara_file_common.h"

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * TYPEDEFS
 */
typedef struct elara_flash_t
{
    int (*read_init)(void);
    void (*write_init)(void);
    void (*read)(uint16 len, uint8 *pBuf);
    uint32 (*if_flash_enough)(uint32 binSize);
    int (*write)(uint16 len, uint8 *pBuf);
    int (*write_done)(void);
    int (*erase)(void);
    void (*lost_connected)(void);

    uint32 (*any_thing_else)(void);
    bool (*if_erase_finished)(void);

    bool (*if_first_256_FF)(void);
} ELARAFLASH;

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */


/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
static ELARAFLASH gFlash = {
    elara_flash_read_init_cc2541,
    elara_flash_write_init_cc2541,
    elara_flash_read_cc2541,
    elara_flash_if_flash_enough_cc2541,
    elara_flash_write_cc2541,
    elara_flash_write_done_cc2541,
    elara_flash_erase_cc2541,
    elara_flash_lost_connected_cc2541,

    elara_flash_any_thing_else_cc2541,
    elara_flash_if_erase_finished_cc2541,
    NULL
};

/*********************************************************************
 * LOCAL FUNCTIONS
 */
/*
 * 0: cc2541,  1:w25q
 */
int elara_flash_common_init(uint8 flag)
{
    if (flag == 0)
    {
        gFlash.read_init = elara_flash_read_init_cc2541;
        gFlash.write_init = elara_flash_write_init_cc2541;
        gFlash.read = elara_flash_read_cc2541;
        gFlash.if_flash_enough = elara_flash_if_flash_enough_cc2541;
        gFlash.write = elara_flash_write_cc2541;
        gFlash.write_done = elara_flash_write_done_cc2541;
        gFlash.erase = elara_flash_erase_cc2541;
        gFlash.lost_connected = elara_flash_lost_connected_cc2541;

        gFlash.any_thing_else = elara_flash_any_thing_else_cc2541;
        gFlash.if_erase_finished = elara_flash_if_erase_finished_cc2541;
        return 0;
    }
    if (flag == 1)
    {
        gFlash.read_init = elara_flash_read_init_spi_w25q;
        gFlash.write_init = elara_flash_write_init_spi_w25q;
        gFlash.read = elara_flash_read_spi_w25q;
        gFlash.if_flash_enough = elara_flash_if_flash_enough_spi_w25q;
        gFlash.write = elara_flash_write_spi_w25q;
        gFlash.write_done = elara_flash_write_done_spi_w25q;
        gFlash.erase = elara_flash_erase_spi_w25q;
        gFlash.lost_connected = elara_flash_lost_connected_spi_w25q;

        gFlash.any_thing_else = elara_flash_any_thing_else_spi_w25q;
        gFlash.if_erase_finished = elara_flash_if_erase_finished_spi_w25q;

        gFlash.if_first_256_FF = elara_flash_if_first_256_FF_spi_w25q;
        return 1;
    }
    return -1;
}

int elara_flash_read_init(void)
{
    osal_pwrmgr_device( PWRMGR_ALWAYS_ON );
    return gFlash.read_init();
}

void elara_flash_write_init(void)
{
    
    gFlash.write_init();
}

/*********************************************************************
 * @fn      elara_flash_if_flash_enough
 *
 * @brief   Before write bin to flash, we should check flash memory big enough.
            (Should run elara_flash_write_init function first.)
 *
 * @param   binSize - The size of the bin
 *
 * @return  The aviliabel flash size, 0 fail.
 */
uint32 elara_flash_if_flash_enough(uint32 binSize)
{
    return gFlash.if_flash_enough(binSize);
}

int elara_flash_erase(void)
{
    return gFlash.erase();
}

bool elara_flash_if_erase_finished(void)
{
    return gFlash.if_erase_finished();
}

bool elara_flash_if_first_256_FF(void)
{
    return gFlash.if_first_256_FF();
}
/*********************************************************************
 * @fn      elara_flash_read
 *
 * @brief   Read data from flash.
 *
 * @param   len - len%256 = 0
 * @param   pBuf - Data is read into this buffer.
 *
 * @return
 */
void elara_flash_read( uint16 len, uint8 *pBuf )
{
    gFlash.read(len, pBuf);
}

/*********************************************************************
 * @fn      elara_flash_write
 *
 * @brief   Writes 'len' bytes to the internal flash.
 *
 * @param   len - len%4 must be zero and len must < HAL_FLASH_PAGE_SIZE
 * @param   pBuf - Valid buffer space at least as big as 'len' X 4.
 *
 * @return
 */
int elara_flash_write(uint16 len, uint8 *pBuf)
{
    return gFlash.write(len, pBuf);
}

int elara_flash_write_done(void)
{
    return gFlash.write_done();
}

// When lost connection or upgrade fail, we should save current active page and offset.
void elara_flash_lost_connected(void)
{
    gFlash.lost_connected();
}

uint32 elara_flash_any_thing_else(void)
{
    return gFlash.any_thing_else();
}
/*********************************************************************
*********************************************************************/
